---
title: 'Kate Scripting: Indentation'
author: Dominik Haumann

date: 2007-07-18T11:17:00+00:00
url: /2007/07/18/kate-scripting-indentation/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/07/kate-scripting-indentation.html
categories:
  - Developers
  - Users

---
Kate Part in KDE4 supports the ECMAScript (JavaScript) language by using [kjs][1]. In KDE3 we had several hard-coded indenters in C++, the idea is to let scripts do all the indentation in KDE4.  
<span style="font-weight: bold;">How does it work?</span> It is similar to vim: You simply create a script in the directory $KDEDIR/share/apps/katepart/jscript. An indentation script has to follow several rules:

  1. it must have a valid script header (the first line must include the string <span style="font-weight: bold;">kate-script</span> and indentation scripts must have the <span style="font-weight: bold;">type: indentation</span>)
  2. it must define some variables and functions

Whenever the user types a character, the flow in Kate Part works like this

  1. check the indentation script&#8217;s <span style="font-weight: bold;">trigger characters</span>, i.e. whether the script wants to indent code for the typed character
  2. if yes, call the indentation function
  3. the return value of the indentation function is an <span style="font-weight: bold;">integer value representing the new indentation depth</span> in spaces.

In the 3rd step there are 2 special cases for the return value:

  1. <span style="font-weight: bold;">return value = -1:</span> Kate keeps the indentation, that is it searches for the last non-empty line and uses its indentation for the current line
  2. <span style="font-weight: bold;">return value < -1</span> tells Kate to do nothing

<span style="font-weight: bold;">So how does a script look like exactly?<span style="font-weight: bold;"><br /> </span></span>The name does not really matter, so let&#8217;s call it foobar.js:<span style="font-weight: bold;"><span style="font-weight: bold;"><br /> </span></span>

<pre>/* kate-script
 * name: MyLanguage Indenter
 * license: LGPL
 * author: Foo Bar
 * version: 1
 * kate-version: 3.0
 * type: indentation
 *
 * optional bla bla here
 */

// specifies the characters which should trigger indent() beside the default '\n'
triggerCharacters = "{}";

// called for the triggerCharacters {} and
function indent(line, indentWidth, typedChar)
{
  // do calculations here
  // if typedChar is an empty string, the user hit enter/return

  // todo: Implement your indentation algorithms here.
  return -1; // keep indentation
}</pre>

More details on the header:

  * name [required]: the name will appear in Kate&#8217;s menus
  * license [optional]: not visible in gui, but should be specified in js-files. it is always better to have a defined license
  * author [optional]: name
  * version [optional]: recommended. an integer
  * kate-version [required]: the minimum required kate-version (e.g. for api changes)
  * type [required]: must be set to indentation

The only missing part is the API which Kate Part exports to access the document and the view. Right now, there is no API documentation, so you have to look at the code:

  * [kdelibs/kate/jscript/katejscript.cpp][2] search for &#8220;<span style="font-weight: bold;">@Begin</span>&#8221; tags

You will find, that the current document exports functions like

  * document.fileName()
  * document.isModified()
  * document.charAt(line, column)
  * etc&#8230;

The view exports functions like

  * view.cursorPosition()
  * view.hasSelection()
  * view.clearSelection()
  * &#8230;

That&#8217;s the boring part of this blog. The interesting one is unfortunately shorter: <span style="font-weight: bold;">we are looking for contributors</span> who want to write scripts or help in the C++ implementation :)

 [1]: http://api.kde.org/4.0-api/kdelibs-apidocs/kjs/html/classes.html
 [2]: http://websvn.kde.org/trunk/KDE/kdelibs/kate/jscript/katejscript.cpp?revision=683169&view=markup