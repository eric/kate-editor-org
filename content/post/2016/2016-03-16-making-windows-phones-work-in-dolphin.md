---
title: Making Windows Phones work in Dolphin
author: Dominik Haumann

date: 2016-03-16T07:28:52+00:00
url: /2016/03/16/making-windows-phones-work-in-dolphin/
categories:
  - Common
tags:
  - planet

---
Hi all,

if you have a Windows Phone (e.g. Lumia or similar), then please help us on <a href="https://git.reviewboard.kde.org/r/127386/" target="_blank">MTP & Windows Phones</a> so we can find the correct patch to make these devices work in the kio-mtp io slave.

Thanks! :-)