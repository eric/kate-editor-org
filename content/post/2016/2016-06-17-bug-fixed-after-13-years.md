---
title: Bug fixed after 13 years
author: Christoph Cullmann

date: 2016-06-17T21:42:44+00:00
url: /2016/06/17/bug-fixed-after-13-years/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
During the Randa sprint an 13 year old bug was fixed :P

I myself moved it to kxmlgui (out of Kate/KWrites&#8217;s harm, good to delegate stuff away :) in 2003 and since then we got a lot of duplicates but never somebody got the time to track it down. (333 votes for it, really nice number)

Now, after 13 years, it got done (and yet another chance that a bug becomes a sentient intelligence has vanished ;=)

<p class="editable">
  <a href="https://bugs.kde.org/show_bug.cgi?id=64754"><strong>Bug 64754 &#8211; <span id="summary_alias_container" class=""><span id="short_desc_nonedit_display">XMLGUI items (toolbar icons, menu items) from merge sections move or disappear</span></span></strong></a>
</p>

<p class="editable">
  Thanks to David, Dominik and Sune to get this fixed ;=)
</p>

<p class="editable">
  And I really like the comment we got after the fix, awesome:
</p>

> <p class="editable">
>   David Faure, you deserve a medal.
> </p>
> 
> <p class="editable">
>   A 13 year old KDE bug finally fixed. Just think, when this bug was first reported:
> </p>
> 
> <p class="editable">
>   &#8212; The current Linux Kernel was 2.6.31
> </p>
> 
> <p class="editable">
>   &#8212; Top Movie? The ORIGINAL Underworld.
> </p>
> 
> <p class="editable">
>   &#8212; Windows XP was the most current desktop verison. Vista was still 3 years away.
> </p>
> 
> <p class="editable">
>   &#8212; Top 2 Linux verions? Mandrake and Redhat (Fedora wouldn&#8217;t be released for another 2 months, Ubuntu&#8217;s first was more than a year away.)
> </p>
> 
> <p class="editable">
>   &#8212; Top billboard song? Crazy In Love by Jay-Z & Beyoncé (who was barely old enough to drink)
> </p>
> 
> <p class="editable">
>   &#8212; 3 of the 5 KDE ev board members hadn&#8217;t ever used KDE
> </p>
> 
> <p class="editable">
>   &#8212; Palestinian Prime Minister Mahmoud Abbas resigns ending &#8220;Road Map for Peace&#8221;
> </p>
> 
> <p class="editable">
>   &#8212; Current KDE version? 3.1.4 (KDE 3 had only been out for a year)
> </p>
> 
> <p class="editable">
>   Very very cool!
> </p>

<p class="editable">
  Not sure if the kernel version is right, more likely some < 2.6.0 kernel was the king of the hill at that time, but otherwise, great :P
</p>

<p class="editable">
  Therefore: <a href="https://www.kde.org/fundraisers/randameetings2016/">Support us</a>, Randa sprint and other sprints really bring our software stack forward! Nowhere else such a nice group of KDE developers can meet up to solve such problems together!
</p>

<p class="editable">
  <a href="https://www.kde.org/fundraisers/randameetings2016/"><img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" /></a>
</p>