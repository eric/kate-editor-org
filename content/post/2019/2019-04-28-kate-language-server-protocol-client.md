---
title: Kate Language Server Protocol Client
author: Christoph Cullmann

date: 2019-04-28T18:36:00+00:00
excerpt: |
  The Language Server Protocol (LSP) allows the integration of stuff like code completion, jump to definition, symbol search and more into an application without manual re-implementation for each language one wants to support.
  LSP doesn&rsquo;t fully all...
url: /posts/kate-language-server-protocol-client/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-language-server-protocol-client/
syndication_item_hash:
  - 6d3b20b396868d18eecb7a2be0214250
categories:
  - Common

---
The [Language Server Protocol (LSP)][1] allows the integration of stuff like code completion, jump to definition, symbol search and more into an application without manual re-implementation for each language one wants to support. LSP doesn&rsquo;t fully allow an integration like [KDevelop][2] or Qt Creator do with the [libclang][3] based tooling aimed for C/C++ but on the other side offers the possibility to interface with [plenty of languages][4] without a large effort on the client side.

If one takes a look at some current [LSP clients list][5], a lot of editors and IDEs have joined the LSP family in the last years.

In the past I was always scared away to start implementing this in Kate, as no readily available library was around to do the low-level work for the client. Whereas you get some reference stuff for the JSON based protocol for JavaScript and such, for Qt nothing was around.

Fortunately Qt Creator started to implement an LSP client beginning with [the 4.8 release][6].

Based on this code, I started now to get this into the Kate project plugin. At the moment not much more has happened then some initial import of the Qt Creator LSP infrastructure code into the [Kate lsp branch][7].

If I get this working (help is welcome!), any improvements will be submitted back to the Qt Creator implementation. If it really starts to work well in Kate, one might think about some better code sharing in the long term. But before such plans, first at least some basic things must work for some initial language server like [clangd][8].

 [1]: https://microsoft.github.io/language-server-protocol/overview
 [2]: https://www.kdevelop.org/
 [3]: https://clang.llvm.org/docs/Tooling.html
 [4]: https://langserver.org/#implementations-server
 [5]: https://langserver.org/#implementations-client
 [6]: https://blog.qt.io/blog/2018/12/06/qt-creator-4-8-0-released/
 [7]: https://cgit.kde.org/kate.git/log/?h=lsp
 [8]: https://clang.llvm.org/extra/clangd/