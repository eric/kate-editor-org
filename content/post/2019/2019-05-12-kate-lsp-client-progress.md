---
title: Kate LSP Client Progress
author: Christoph Cullmann

date: 2019-05-12T21:54:00+00:00
excerpt: |
  The Kate lsp branch contains now the infrastructure as used by Qt Creator.
  In addition, clangd is now somehow started in a working state for the first project opened inside Kate.
  For example, if you use the CMake Kate project generator and you compile ...
url: /posts/kate-lsp-client-progress/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-lsp-client-progress/
syndication_item_hash:
  - 71278f4f26101b2a768b8adafbd8f6d8
  - 1f05b24a073dd45a321a374afbca59c4
categories:
  - Common

---
The [Kate lsp branch][1] contains now the infrastructure as used by Qt Creator. In addition, [clangd][2] is now somehow started in a working state for the first project opened inside Kate.

For example, if you use the CMake Kate project generator and you compile Kate from the &ldquo;lsp&rdquo; branch, clangd should pick up the compile_commands.json for a CMake generated Kate project.

;=) Unfortunately not much more than starting and informing clangd about the open workspaces (for the first opened project) works ATM.

If you press ALT-1 over some identifier, you will get some debug output on the console about found links, like below:

> qtc.languageclient.parse: content: &ldquo;{\&ldquo;id\&rdquo;:\&ldquo;{812e04c6-2bca-42e3-a632-d616fdc2f7d4}\&ldquo;,\&ldquo;jsonrpc\&rdquo;:\&ldquo;2.0\&ldquo;,\&ldquo;result\&rdquo;:[{\&ldquo;range\&rdquo;:{\&ldquo;end\&rdquo;:{\&ldquo;character\&rdquo;:20,\&ldquo;line\&rdquo;:67},\&ldquo;start\&rdquo;:{\&ldquo;character\&rdquo;:6,\&ldquo;line\&rdquo;:67}},\&ldquo;uri\&rdquo;:\&ldquo;file:///local/cullmann/kde/src/kate/kate/katemainwindow.h\&ldquo;}]}&rdquo;

The current ALT-1 handling is a big hack, as then one just adds the current document and triggers the GotoDefinitionRequest. A proper implementation tracks the opened/closed documented of the editor.

But at least in principle Kate is now able to start some language server processes and talk a bit with them, all thanks to the nice code borrowed from Qt Creator.

:=) As my spare time is limited, any help in bringing the branch up-to-speed is highly welcome, just drop us a mail to <kwrite-devel@kde.org> or mail me in private (<cullmann@kde.org>). A working LSP integration will help to make Kate more attractive for programmers of many languages.

 [1]: https://cgit.kde.org/kate.git/log/?h=lsp
 [2]: https://clang.llvm.org/extra/clangd/