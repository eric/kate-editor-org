---
title: KSyntaxHighlighting - Over 300 Highlightings...
author: Christoph Cullmann
date: 2019-08-25T14:06:00+02:00
url: /post/2019/2019-08-25-ksyntaxhighlighting-over-300-highlightings/
---

I worked yesterday again on the [Perl script](https://invent.kde.org/websites/kate-editor-org/blob/master/update-syntax.pl) that creates the highlighting update site used by e.g. Qt Creator.

I thought it would be perhaps a good idea to create some simple human readable overview with all existing highlighting definitions, too.

The result is this auto-generated [Syntax Highlightings](/syntax/) page.

Astonishing enough, at the moment the script counts 307 highlighting definitions.
I wasn't aware that we already crossed the 300 line.

Still, it seems people miss some highlighting definitions, take a look at the [bug list of KSyntaxHighlighting](https://bugs.kde.org/buglist.cgi?bug_status=__open__&component=syntax&list_id=1657555&product=frameworks-syntax-highlighting).

The bugs with requests for new definition requests got marked with **[New Syntax]**.

I am actually not sure if we should at all keep bugs for such requests, if no patch is provided there to add such an highlighting.
Obviously, we want to have proper highlighting for all stuff people use.

But, as we have these bugs at the moment, if you feel you have the time to help us, take a look.
Some Perl 6 highlighting is appreciated, or any of the others there ;=)
Or perhaps you have a own itch to scratch and provide something completely different!

[Our documentation](https://docs.kde.org/stable5/en/applications/katepart/highlight.html) provides hints how to write a highlighting definition.
Or just take a look at the existing XML files in our [KSyntaxHighlighting repository](https://cgit.kde.org/syntax-highlighting.git/).

Patches are welcome on the [KDE Phabricator](https://phabricator.kde.org/).

Otherwise, if you provide a new definition XML + one test case, you can attach them to the bug, too (or send them to kwrite-devel@kde.org).

With test case an example file in the new language is wanted, with some liberal license.
This will be used to store reference results of the highlighting to avoid later regressions and to judge the quality of the highlighting and later improvements.

We would prefer MIT licensed new files, if they are not derived from older files that enforce a different license, thanks!
(in that case, it would be good to mention in some XML comment which file was used as base)
