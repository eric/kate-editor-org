---
title: Kate - Initial Rust LSP support landed!
author: Christoph Cullmann
date: 2019-08-07T21:02:00+02:00
url: /post/2019-08-07-kate-initial-rust-lsp-support/
---

Initial support for the [rls](https://github.com/rust-lang/rls) Rust LSP server has landed in kate.git master.
The matching rls issue about this can be found [here](https://github.com/rust-lang/rls/issues/1521).

Given I am no Rust expert, I can only verify that at least some operations seem to work for me if the Rust toolchain is setup correctly ;=)

The current experience isn't that nice as with clangd, for example I get no symbols outline here.
What is possible with clangd can be seen [in one of my previous posts, video included](/posts/kate-lsp-status-july-22/).

Any help to improve this is welcome. The patch that landed can be viewed [here](https://cgit.kde.org/kate.git/commit/?id=9838d5decb94034535e2433213bcdd902650e9ba), lend a hand if you know how to fix up stuff!
