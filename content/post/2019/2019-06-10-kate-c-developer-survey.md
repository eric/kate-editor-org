---
title: 'Kate & C++ Developer Survey'
author: Christoph Cullmann

date: 2019-06-10T20:45:00+00:00
excerpt: |
  While browsing the ISO C++ homepage I stumbled over the results PDF of the Second Annual C++ Foundation Developer Survey “Lite”.
  I was astonished that Kate made it into the &ldquo;Which development environments (IDEs) or editors do you use for C++ deve...
url: /posts/kate-cpp-developer-survey/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-cpp-developer-survey/
syndication_item_hash:
  - fb534baaef580fbae967bd89bae8d808
categories:
  - Common

---
While browsing the [ISO C++ homepage][1] I stumbled over the [results PDF][2] of the [Second Annual C++ Foundation Developer Survey “Lite”][3].

I was astonished that Kate made it into the &ldquo;Which development environments (IDEs) or editors do you use for C++ development?&rdquo; results.

;=) Seems not only I use it as my normal editor for working on C++ code.

And heads up, KDevelop is there, too!

This is actually the second survey, I missed to notice the first one last year. The [results PDF][4] from [last year][5] shows Kate did show up already there.

We shouldn&rsquo;t be that proud of only having less than three percent usage, but still, we are there at all. I hope we can rise here a bit in the future.

At least we work on supporting modern C++ standards in the highlighting, e.g. see the recent C++20 improvements ([Phabricator][6] + [Commit][7]). Thanks to Jonathan Poelen for the patch!

[Language Server Protocol (LSP)][8] would help, too, but my initial work is far from usable, help appreciated. Beside a hacked &ldquo;goto definition&rdquo; nothing works at the moment (and it is hardcoded to just start clangd, no other LSP server).

 [1]: https://isocpp.org/
 [2]: https://isocpp.org/files/papers/CppDevSurvey-2019-04-summary.pdf
 [3]: https://isocpp.org/blog/2019/05/results-summary-2019-global-developer-survey-lite
 [4]: https://isocpp.org/files/papers/CppDevSurvey-2018-02-summary.pdf
 [5]: https://isocpp.org/blog/2018/03/results-summary-cpp-foundation-developer-survey-lite-2018-02
 [6]: https://phabricator.kde.org/D21585
 [7]: https://cgit.kde.org/syntax-highlighting.git/commit/?id=9ba02971123d255c10fca97223538c439e252e3d
 [8]: https://cullmann.io/posts/kate-lsp-client-progress/