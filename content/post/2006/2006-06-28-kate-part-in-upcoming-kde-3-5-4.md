---
title: Kate Part in upcoming KDE 3.5.4
author: Dominik Haumann

date: 2006-06-28T19:03:00+00:00
url: /2006/06/28/kate-part-in-upcoming-kde-3-5-4/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/06/kate-part-in-upcoming-kde-354.html
categories:
  - Common

---
The last week was highly productive for Kate Part, as the following bugs were fixed:

  * [89042][1] while pressing &#8220;del&#8221; key kate crashes (crash, bt)
  * [103648][2] Codefolding Crash &#8211; Reproducable
  * [118584][3] scroll position not upgrading (dynamic word wrap)
  * [119435][4] kate crash when a file is saved
  * [123315][5] kwrite/kate crashes randomly after save
  * [124102][6] changing syntax highlighting when code is folded crashes katepart
  * [127928][7] kate crashes deleting a block of text
  * [128690][8] Dynamic word wrap makes text input slow
  * [129853][9] Horizontal scrollbar and view not synced, if dynamic and static word wrap are off 
  * and some minor issues

That are 6 crash fixes. Kate Part in KDE 3.5.4 will be more stable than ever :) That&#8217;s especially cool for KDevelop, Quanta+, Kile &#8211; well and Kate.  
Special thanks to Andreas Kling for initiating the bug squashing sessions! You are like a blackbox: The input is a bug and your output is the fix ;)

 [1]: http://bugs.kde.org/show_bug.cgi?id=89042
 [2]: http://bugs.kde.org/show_bug.cgi?id=103648
 [3]: http://bugs.kde.org/show_bug.cgi?id=118584
 [4]: http://bugs.kde.org/show_bug.cgi?id=119435
 [5]: http://bugs.kde.org/show_bug.cgi?id=123315
 [6]: http://bugs.kde.org/show_bug.cgi?id=124102
 [7]: http://bugs.kde.org/show_bug.cgi?id=127928
 [8]: http://bugs.kde.org/show_bug.cgi?id=128690
 [9]: http://bugs.kde.org/show_bug.cgi?id=129853