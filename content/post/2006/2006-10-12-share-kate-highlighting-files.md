---
title: Share Kate Highlighting Files
author: Dominik Haumann

date: 2006-10-12T21:35:00+00:00
url: /2006/10/12/share-kate-highlighting-files/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/10/share-kate-highlighting-files.html
categories:
  - Common

---
Thanks to Frank we have a [dedicated section for Kate Highlighting files on kde-files.org][1] now. We encourage everyone to publish their .xml files there. The Kate team agreed to add more highlighting files to the official Kate releases, though, as long as they are of general use. For KDE4 we plan to have a section for Kate Indentation scripts, too. But that&#8217;s content for another blog, which will follow later :)

 [1]: http://kde-files.org/