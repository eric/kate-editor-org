---
title: Indentation and Coding Style
author: Dominik Haumann

date: 2006-07-23T08:56:00+00:00
url: /2006/07/23/indentation-and-coding-style/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/07/indentation-and-coding-style.html
categories:
  - Common

---
[kdelibs will have coding style conventions][1]. In general. this is not a bad idea. Our 45528 slocs in KatePart all use a <span style="font-style: italic;">consistent</span> indent-width of 2 spaces. Changing this does not really make sense &#8211; ok, if svn praise -w (sure, we never need svn blame in our code ;) finally works, we can discuss this again.

In other words: How &#8220;consistent&#8221; will kdelibs get with this new conventions? The interesting part of Zack&#8217;s mail is

> [&#8230;]  
> No exceptions. Either everything or nothing  
> [&#8230;]

While everything would really be cool, in truth it&#8217;s not possible to achieve, is it? We will have exceptions or rather violations, I&#8217;m pretty sure :)

Some developers already start adding <a href="/?p=236" target="_self">katepart modelines</a> to their souce code. For those who don&#8217;t know yet: You also can simply use a single <a href="/?p=244" target="_self">.kateconfig file</a>, which is the equivalent to emacs dir config files. You just have to make sure to set the search depth for a .kateconfig file to &#8211; let&#8217;s say &#8211; 3. Do this in

> Settings > Configure Kate&#8230; > Open/Save

 [1]: http://lists.kde.org/?l=kde-core-devel&m=115340952719876&w=2