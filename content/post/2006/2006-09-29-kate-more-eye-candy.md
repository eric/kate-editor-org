---
title: 'Kate: More eye-candy'
author: Dominik Haumann

date: 2006-09-29T17:34:00+00:00
url: /2006/09/29/kate-more-eye-candy/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/09/kate-more-eye-candy.html
categories:
  - Common

---
Two years ago we had the KDE conference in Ludwigsburg, Germany. At that time, Martijn Klingens committed a patch to [show trailing spaces][1] in Kate. I just change the visualization a bit, the change is in for KDE4 :) Tabs are marked with a &#8216;»&#8217; character, and spaces with a dot. On the left you can see the old and on the right the new version; Here is a screenshot:  
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://photos1.blogger.com/blogger/4106/1694/1600/tabsspaces.0.png"><img style="cursor: pointer;" src="http://photos1.blogger.com/blogger/4106/1694/400/tabsspaces.0.png" alt="" border="0" /></a>

 [1]: http://lists.kde.org/?l=kwrite-devel&m=109424792916463&w=2