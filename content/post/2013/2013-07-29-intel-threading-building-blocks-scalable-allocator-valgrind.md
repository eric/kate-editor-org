---
title: 'Intel Threading Building Blocks Scalable Allocator & Valgrind'
author: Christoph Cullmann

date: 2013-07-29T18:01:56+00:00
url: /2013/07/29/intel-threading-building-blocks-scalable-allocator-valgrind/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
tags:
  - planet

---
Hi,

if you ever use the TBB (<a href="http://threadingbuildingblocks.org/" target="_blank">Intel Threading Building Blocks</a>) allocator to overwrite malloc/free/* and want to use <a href="http://www.valgrind.org/" target="_blank">Valgrind</a> for leak checking and fail, here is the simple trick to get it working:

<pre>valgrind --soname-synonyms=somalloc=\*tbbmalloc\* &lt;your-application-here&gt;</pre>

I missed that hint in the Valgrind documentation for my first tries ;)

Btw., the scalable allocator from TBB is a really BIG improvement over the normal system allocator on current Linux & Windows machines if you allocate mostly fixed size small object, like what happens if you heavily use STL data structures like std::map/set that are implemented as trees and you have other stuff like DOM/AST like data structures (even in the single threaded case, for which it just saves a LOT of memory).