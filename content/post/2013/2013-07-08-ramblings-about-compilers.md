---
title: Ramblings about compilers…
author: Christoph Cullmann

date: 2013-07-08T20:03:52+00:00
url: /2013/07/08/ramblings-about-compilers/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
tags:
  - planet

---
In my job I work on binary and source level analysis software running on Linux and Windows. One of my tasks is to maintain the build farm and compile environment, therefore I am responsible for keeping care of the compilers and libraries we use (like the beloved <a href="http://qt-project.org" title="Qt Project" target="_blank">Qt</a>, congratulations for the nice 5.1 release, btw.).

For Linux, we normally use the <a href="http://gcc.gnu.org" title="The GNU Compiler Collection" target="_blank">GNU C/C++ compiler</a>. That works out very well since years. We have hit compiler bugs only a few times and there was always already some patch-release out in the wild that fixed our issues. In addition the GCC developers brought a constant stream of improvements in the area of C and C++ standard compliance. I am more than happy with the GCC quality.

I did some brief experiments with <a href="http://www.llvm.org/" title="LLVM Project" target="_blank">LLVM/Clang</a> two years ago and was not that impressed in the stability of the C++ support (especially of the fresh libc++ STL implementation) but even for a such young project, it was fast as light to get issues fixed, like a crash in the unordered containers (see <a href="http://llvm.org/bugs/show_bug.cgi?id=10278" title="libc++ Bug 10278" target="_blank">Bug 10278</a>, only some hours from report to fix, even if it was only a trivial size variable used uninitialized in the libc++ implementation). I guess today LLVM would perform ok to compile our software (including Qt), still I will stick to GCC, given we use some libraries that normally only get love for that compiler on Linux (at least at the moment).

But now, after my experiences with open source compilers, lets take a look at the wonderful world of Windows.

I know for Windows GCC variants are available (like the 32 bit MinGW-builds shipped with some Qt library variant) and some experimental work on some LLVM able to compile applications using Windows SDK headers is ongoing. Still, the most common way on Windows is to use the Visual Studio compiler and given we always have some libraries of 3rd-parties around that we need to use and that are Visual Studio compiled the Microsoft compiler is more or less unavoidable.

Now, lets take a look of this fabulous compiler you get there. At the moment two major Version are of interest, the 2010 and 2012 variant of Visual Studio.

As I started to evaluate Visual Studio 2010 in 2010, I was immediately hit by <a href="http://support.microsoft.com/kb/2280741/en-us" title="2010 x64 Bug" target="_blank">its nice 64 bit optimizer bug</a>. Not a single Qt application would start up without immediate segfault&#8230; More about this <a href="https://connect.microsoft.com/VisualStudio/feedback/details/573323/optimizer-bug-in-vc-2010-64bit" title="More about x64 optimizer bug" target="_blank">here</a> and even in <a href="https://bugreports.qt-project.org/browse/QTBUG-11445" title="Qt Bug about x64 optimizer" target="_blank">the Qt bugtracker</a>, especially the answers from the support are interesting, it seems to be forbidden to return a object per value from a function ;)

From report to &#8220;hotfix&#8221; it has taken some months, that means with the release version of 2010, you couldn&#8217;t build any optimized x64 application using Qt (and I guess most other evil libraries using object on the stack that got misaligned by the optimizer) for some months.

Afterwards I was that adventurous to install the <a href="http://www.microsoft.com/en-us/download/details.aspx?id=23691" title="2010 SP1" target="_blank">SP1</a> to get the included further compiler fixes. Unfortunately that meant I got all my x64 compilers deleted, but no problem, just close to a month later (until which you need to reinstall your Visual Studio/SDK without SP1 and reapply you hotfix) you got again <a href="http://www.microsoft.com/en-us/download/details.aspx?id=23691" title="Get x64 compilers back" target="_blank">some fix</a> that reinstalls the compilers that SP1 will remove. Really, you will need to install the 2010 + SP1 + then a fix that restores the compilers SP1 removes if you want to have a working VS 2010 Express for x64 (or stay without SP1 and use the above hotfix for the x64 compiler).

For Visual Studio 2012, which I started to eval months ago, you will get the same story, but reversed. Now we have some nice and nasty <a href="https://bugreports.qt-project.org/browse/QTBUG-32159" title="QT Issue about 2012 x86 issue" target="_blank">x86 optimizer bug</a>. That bug is reported since long see <a href="https://connect.microsoft.com/VisualStudio/feedback/details/780362/optimization-bug-of-range-comparison" title="More on the x86 optimizer bug" target="_blank">here</a>, it leads to problems for software that use <a href="http://site.icu-project.org/" title="ICU" target="_blank">libicu</a>, see the matching <a href="http://bugs.icu-project.org/trac/ticket/9693" title="ICU bug" target="_blank">icu bug</a>. But given the simple code pattern that can make it occur, it may hit other parts, too. Until now, no fix, only some &#8220;will be fixed in next version&#8221;&#8230;

For me that means, I need to stick with 2010 for x86 and with 2012 for x64, given I want to use the pre-build Qt binaries and not to dive into the &#8220;compile qt on windows&#8221; fun once more. I would like 2012 for x86, too, to benefit from the C++11 feature improvements and to not have to support two different compiler variants, but I guess that will have to wait, seems its not that easy to build some working x86 compiler that can do optimizations.

Therefore, really, +1 to the compiler developers of the GCC and LLVM project. Its amazing what they have archived!  
And +1 for the improvements in the C99 and C++ area in Visual Studio, too. Still I really would be more than happy to get a fix for the x86 optimizer issue in VS 2012. Even no report that it is really fixed in 2013 until now, but I am not sure that was really tried, I didn&#8217;t try it myself because I am lazy and must blog instead ;=)