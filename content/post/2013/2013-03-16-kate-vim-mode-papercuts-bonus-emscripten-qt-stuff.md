---
title: 'Kate Vim Mode: Papercuts (+ bonus emscripten-qt stuff!)'
author: Simon St James

date: 2013-03-16T14:58:23+00:00
url: /2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/
pw_single_layout:
  - "1"
categories:
  - Common

---
I was a long time Vim holdout and often wondered to myself, &#8220;[just why do those nutheads use vim?][1]&#8221; until I googled and read the blog I just linked. After trying Vim out for a few days, I was completely hooked, but didn&#8217;t want to leave KDevelop with its awesome Intellisense/ code completion/ navigation features. Thankfully, Kate has a Vim mode, so I could get the best of both worlds :)

However, I&#8217;d often run into small but jarring inconsistencies with Vim, or sometimes find missing features, and so I set about trying to fix as many as I could. Since these were usually very small and unimportant things, I&#8217;ll just go ahead and list them, Graesslin-style :)

Vim mode is one of those rare and happy pieces of code which you can very easily write tests for: just add the starting text; the list of keypresses; and the expected, final text. Kuzmich Svyatoslav wrote a big battery of them a while back, and my [first ever patch][2] was an incredibly trivial little one that fixed a hole in the test framework and a bug that the hole obscured.

After that, I:

  * Improved the test framework to enable us to test Insert Mode features;
  * Fixed the inner curly bracket text object to be more like Vim (rather than deleting everything between the curly braces on a **diB**, Vim usually attempts to leave the closing curly bracket on its own line);
  * Added **{** and **}** motions to Visual Mode;
  * Fix a few bugs with Erlend&#8217;s very handy &#8220;comma text object&#8221; extension;
  * Fixed numerous bugs with **t**/**T** and **,** & **;** involving them getting &#8220;stuck&#8221; behind occurrences of the character to search for, and also made them &#8220;counted&#8221; so that one can do e.g. **3ta**, and also mirroring Vim&#8217;s behaviour where if we can&#8217;t find the required count of characters on the line, we don&#8217;t move at all;
  * Stopped the transition from Visual Mode to Normal mode from resetting the cursor position if we exited via _ESC_ or _Ctrl-c_;
  * Fixed a bug with the nifty &#8220;block append&#8221; feature;
  * Fixed a nice bug with replaying the last command via **.** when the last command was an insertion that had a _Ctrl-o_ in the middle of it (all the characters after we exited the _Ctrl-o_ were treated as commands, with predictably zany results!)
  * Ensured that the bracket text object worked in Visual Mode;
  * Made the very handy Key Mapping feature &#8220;counted&#8221;, so you could perform your chosen Mapping several times in a row, and also made them count as one edit that could be reverted with just one request to Undo;
  * Made **p** & **P** move the cursor to the end of the pasted text instead of leaving it at the beginning (whoops!), and similarly for **gp**/**gP** which places the cursor one past the end, hopefully respecting Vim&#8217;s slightly obscure exceptions and corner cases (e.g. **p** and **P** act as **gp** and **gp** when in Temporary Normal Mode);
  * When doing _Ctrl-o_ in Insert Mode, move the cursor back one step if we are at the end of the line;
  * Try to keep the length of the number under the cursor unchanged on _Ctrl-x_ and _Ctrl-a_ and also don&#8217;t let the cursor stray off the number;
  * Respect Vim&#8217;s rules for counting with _Ctrl-x_ and _Ctrl-a_, especially with repeats;
  * Fix a lovely bug with some of the key parsing where the repeat last change command did not work after a _Ctrl-a_;
  * Made inserts (**i**, **I**, **a**, **A**, **o**, **O** and block append) counted, so you can do e.g. **100oHello, World!_<ESC>_**, although I now see that the old implementation of counted **o** and **O** was &#8220;by design&#8221;. Oops &#8211; I&#8217;ll have to sort that out at some point :/;
  * Make _Ctrl-c_ reset the parser so that e.g. **d_<Ctrl-c>_** aborts the deletion. This one was cool as it remained undetected for ages by a sheer fluke: the only visible signs were that after doing e.g. **d_<Ctrl-c>_**, the cursor would stay at half size, and also another bug that I had been promising myself I&#8217;d investigate for a while: typing **r_<Ctrl-c>_** would replace the current character with a nonsense character, which turned out to be the representation of the keypress _Ctrl-c_!;
  * Fixed a few minor flaws where after e.g. **osometext_<ESC>_**, we would have to request an Undo \*twice\* before getting back to where we issued the command;
  * Arranged for a return to Normal Mode if we _Ctrl-c_ or _ESC_ when in Temporary Normal Mode. Vim is weird, here: its behaviour is just as you would expect if you were in Normal Mode, but the status bar still claims we are in Insert Mode!
  * Fixed a bug where issuing **y%** would actually move the cursor as well as yanking the text;
  * Added the last edit markers, **[**,**]** and **.**;
  * Fixed a few bugs with inner bracket text objects being treated as invalid if there were less than three characters long;
  * Respected Vim&#8217;s rules for the **[[**, **][** etc motions when used with commands: they behave slightly differently than when they are used as cursor motions;

and that&#8217;s about it! Kate Vim&#8217;s testability make it very pleasant to develop features/ fixes Test First, so all of these are buttressed by a decent amount of automated tests.

## The Future

I&#8217;m nearing the end of the papercuts stuff, and will shortly move onto slightly larger scale things. This will consist of implementing a Vim-style search and replace bar, with all the usual Vim shortcuts: _Ctrl-c_ to dismiss; _Ctrl-r_ to insert contents of register; _Ctrl-rCtrl-w_ to insert the word under the cursor; etc, and with proper Vim style searching e.g. positioning the cursor on the first letter of the current match; return to where we were if cancelled; interactive search and replace with Y/N/Q/A; etc. I think I&#8217;ll use &#8220;smartcase&#8221; for deciding case sensitivity, but might make this configurable through a hidden config option if there is any demand for it. 

There&#8217;s currently quite a lot of Vim-specific code that has leaked into Kate&#8217;s own Find/ Replace dialog, and I&#8217;d really like to be able to delete that and just have Vim use this Vim-specific version. What do people think? I&#8217;d have to get it at least as functional as Kate&#8217;s own one before I&#8217;d consider this, so we&#8217;d need e.g. some way of going through the Find and the Replace history separately (I don&#8217;t think stock Vim can do this?) and easily toggling whether this is a regex or a literal search, etc. I&#8217;d really like some feedback on this, and a suggestion for shortcuts etc!

Done properly, this would give us a powerful, keyboard-friendly Find (and Command Line) dialog where we can easily track the keypresses, which would set us up nicely for the next major task &#8211; [Vim Macro support!][3]

Before all that, though, I just have a couple of smaller odds and ends to sort out: I need to get auto-completion properly working with **.** (currently, if auto-completion is automatically invoked, pressing **.** won&#8217;t include the completed text), and it would be nice if _Ctrl-p_ and _Ctrl-n_ could wrap around in the list of completions as they do in Vim (need to sort out a Kate bug with completion first). I also have an another [idea][4] which I&#8217;d like some feedback on; it would be nice if people would try it out and let me know if they find it helpful, annoying, etc. If only there was a way of letting people test out a patch that hasn&#8217;t been committed yet &#8230; ! Which leads nicely on to:

## emscripten-kate :)

You may have seen my [blogs][5] on [emscripten-qt][6], a port of Qt to [Emscripten][7], an awesome technology that allows one to compile C/C++ to Javascript. This caught the eye of Mozilla, who were looking for an example of a &#8220;serious&#8221; app to go along with the games and utilities in their current list of Emscripten demos, and they kindly offered to sponsor me to work on this. After looking through a list of candidate apps, they eventually suggested porting Kate: not exactly an easy task, as Kate obviously requires not only a big chunk of kdelibs (with its reliance on DBUS and external processes like kded, etc) but also QtScript for its extensibility (the automatic code indentation for various programming languages, for example, is implemented in JS) meaning I&#8217;d need to get a C++-based Javascript interpreter ported to Javascript ;) 

This ended up being not as hard as I feared, and now we have a decent (though very cut-down; there are no plugins, so it&#8217;s more like KWrite than Kate ;)) version of Kate ported to Javascript. I&#8217;ve merged in the patch I&#8217;d like feedback on as mentioned above so you can easily [try it out][8] as long as you have a modern browser, plenty of bandwidth (even after server-side compression, you&#8217;re still looking at a tens of MB download :/) and also plenty of free RAM and CPU. There&#8217;s an unfortunate [bug][9] in Chrome which means you&#8217;ll need to use the [no icons][10] version, alas. Konqueror and reKonq also do not work, to my knowledge. Let me know what you think, and if anyone has any thoughts on shortcuts for the extended Find/ Command Line bar, do let me know!

 [1]: http://www.viemu.com/a-why-vi-vim.html
 [2]: https://git.reviewboard.kde.org/r/106839/
 [3]: https://bugs.kde.org/show_bug.cgi?id=288351
 [4]: https://git.reviewboard.kde.org/r/109372/
 [5]: http://ssj-gz.blogspot.co.uk/2013/01/emscripten-qt-progress-faster-better.html
 [6]: http://vps2.etotheipiplusone.com:30176/redmine/projects/emscripten-qt/wiki/Demos
 [7]: https://github.com/kripken/emscripten/wiki
 [8]: http://vps2.etotheipiplusone.com:30176/redmine/emscripten-qt-examples/kate-testing/kate.html
 [9]: http://code.google.com/p/chromium/issues/detail?id=180301&thanks=180301&ts=1362512388
 [10]: http://vps2.etotheipiplusone.com:30176/redmine/emscripten-qt-examples/kate-testing/kate-noicons.html