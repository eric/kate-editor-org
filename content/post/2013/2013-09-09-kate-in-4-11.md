---
title: Kate in 4.11
author: Dominik Haumann

date: 2013-09-09T10:06:26+00:00
url: /2013/09/09/kate-in-4-11/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet
  - python
  - vi input mode

---
Another release cycle gone, and the <a title="KDE SC 4.11" href="http://kde.org/announcements/4.11/" target="_blank">KDE Software Compilation 4.11</a> is out in the open (well, for quite some time already), and with that it is time to talk about what changed in Kate the last half year since the [4.10 release][1]. Besides the usual bug fixing (<a title="Kate Bug Fixes since KDE 4.10" href="https://bugs.kde.org/buglist.cgi?list_id=750185&resolution=FIXED&resolution=WORKSFORME&resolution=UPSTREAM&resolution=DOWNSTREAM&query_format=advanced&bug_status=RESOLVED&bug_status=CLOSED&longdesc=4.11&longdesc_type=allwordssubstr&product=kate" target="_blank">~50 bugs</a> since 4.10), the following sections present some major improvements and features of Kate in 4.11.

### Python Plugins

Since KDE 4.10, Kate features the Python »Pâté« plugin. This plugin basically wraps the API of all Kate application interfaces. Therewith, together with the Python KDE (PyKDE) bindings you can develop full-featured Kate plugins in Python. Being relatively new, there are quite some Python plugins available already now (see [this blog][2], [this blog][3], or [this blog][4]).

@Python community: You can interpret this as an invitation for writing lots of cool addons for the Kate text editor in python. All contributions are usually [very much welcome][5]!

### Vi Input Mode

Enabling the [vi input mode][6] turns Kate into a full-fleged vim compatible editor: modal editing exactly like in vim. This mode is especially suited for vim users who want a fully KDE integrated text editor with all the beloved vim features. Started as GSoC project in 2010, the vi input mode evolved over the years to become a very mature alternative to vim itself. However, there is always room for improvements. The good news is, that Kate Part&#8217;s vi input mode gained a lot of attention thanks to Simon, mostly in the form of small [&#8220;papercuts&#8221;][7] that fixed small bugs and annoyances and made behaviour more compatible with Vim, including:

  * Yank highlighting, which helps you to see what text you just yanked;<img class="aligncenter" src="/wp-content/uploads/2013/09/kateyankhighlight.gif" alt="" /> 
  * &#8220;Yank to clipboard&#8221; (courtesy of Fabian Kosmale!);
  * The &#8220;last edit&#8221; markers, &#8220;.&#8221;, &#8220;[&#8221; and &#8220;]&#8221;;
  * Recursive vs non-recursive mappings, for if you want to e.g. map &#8220;j&#8221; to &#8220;gj&#8221; &#8230;
  * &#8230; plus plenty of fixes to gj and gk;
  * and countless other small tweaks to things like the &#8220;inner block&#8221; text object; cursor position after paste; fixes to ctrl-a/x (add to/ subtract from number under cursor); making more motions available in Visual Mode and more motions counted, etc!

One fairly major change is the introduction of an experimental Emulated Vim Command Bar. This, as you will probably have guessed, is a replacement for the current Search dialog that behaves more like Vim&#8217;s, allowing one to e.g. insert the contents of registers via ctrl-r; dismiss via ctrl-c/ ctrl-[ as well as ESC; work properly with both forward (&#8220;/&#8221;) and backward (&#8220;?&#8221;) incremental searches; use smart case for case-sensitivity; allow the use of Vim-style regex&#8217;s etc. As an extension to Vim, I&#8217;ve also added the ability to auto-complete words from the document via ctrl-space: not that useful for searching, but very useful for when you want to do a search+replace command (command mode (&#8220;:&#8221;) did not make it into 4.11, alas).  
<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimsearch.gif" alt="" /> 

In 4.11, it is disabled by default as it is not yet ready for primetime, but may be enabled by setting the hidden config option &#8220;Vi Input Mode Emulate Command Bar&#8221; to &#8220;true&#8221;. It is much more powerful and featureful in current master (see the blog [here!][8]) and will likely be the default in 4.12.

### New Text Folding

Back in [KDE SC 4.8][9], Kate got a new code folding implementation as part of the yearly GSoC project. Compared to the previous implementation, the amount of bugs indeed reduced, but during the 4.9 and 4.10 release it became apparent, that the code folding was still not where it should be. Maybe writing a good code folding implementation was also a bit too much in terms of a newby developer in a GSoC project.

So Christoph sat down and wrote it completely new from scratch, this time [very clean][10] and [simple][11]. And as a new feature, the text folding is now per view, so the folded parts can be different in each view of a document. And as another feature for those who do not use text folding at all: Not using text folding means you have zero overhead, no matter how large the file. Besides that, code folding in Python works now better than ever before, in fact, it should be bug-free.

### Passive Notifications & KTextEditor Interfaces

[Passive notifications][12] are meant to provide a non-intrusive way showing notifications (replacing <a title="KPassivePopup API documentation" href="http://api.kde.org/4.10-api/kdelibs-apidocs/kdeui/html/classKPassivePopup.html" target="_blank">KPassivePopup</a>). Used e.g. by the <a title="Data Recovery in KDE 4.10" href="/2012/10/25/data-recovery-in-4-10/" target="_blank">data recovery</a>, these notifications were also introduced for the [search & replace feature][13] (most of this is already available in KDE SC 4.10.3). In KDE SC 4.11, the interface for showing these passive notifications got officially included into the <a title="KTextEditor::MessageInterface" href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1MessageInterface.html" target="_blank">KTextEditor interfaces</a>, meaning that applications like Kile or KDevelop can show notifications as well. (Issues in KDE 4.11.0 should be fixed for 4.11.1).

Next to the KTextEditor::MessageInterface, Kate has a so-called <a title="KTextEditor::TextHintInterface" href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1TextHintInterface.html" target="_blank">TextHintInterface</a> since year 2003, but it seems its implementation never got finished. For 4.11, Sven Brauch stepped up and finally fixed the <a title="TextHintInterface" href="http://commits.kde.org/kate/93da1d1d18ea594be9fdb42a9789247de0f18397" target="_blank">text hint interface</a>. So showing <a title="TextHintInterface in action" href="http://scummos.blogspot.de/2013/08/gsoc-collaborative-text-editing-in-kate.html" target="_blank">tool tips for text under the mouse</a> (section Text tooltips) is as easy as it was never before:

<img class="size-full wp-image-2690 aligncenter" title="KTextEditor::TextHintInterface showing a tool tip" src="/wp-content/uploads/2013/08/tooltips.png" alt="" width="393" height="137" srcset="/wp-content/uploads/2013/08/tooltips.png 393w, /wp-content/uploads/2013/08/tooltips-300x104.png 300w" sizes="(max-width: 393px) 100vw, 393px" /> 

### Saving Files

When saving files, Kate uses the class <a title="KSaveFile" href="http://api.kde.org/4.11-api/kdelibs-apidocs/kdecore/html/classKSaveFile.html" target="_blank">KSaveFile</a> (in Qt5 available as QSaveFile thanks to David Faure). In short, to avoid data loss, KSaveFile saves the file to a temporary file in the same directory as the target file, and on successful write finally moves the temporary file to the target filename. If the folder of the file does not allow creating new files, KSaveFile automatically falls back to writing the file directly. In this case, data loss could happen if e.g. the system crashes in this moment, but saving the file would otherwise be not possible at all.

### What Comes Next?

Announced just a few days ago, the KDE community prepares for <a title="KDE Release Plan" href="http://dot.kde.org/2013/09/04/kde-release-structure-evolves" target="_blank">KDE Frameworks 5 and much more</a>: The KDE libraries and the KDE Plasma Workspace are now in long term support mode, meaning that bugs will get fixed for another two years. That means that the KTextEditor interfaces are frozen as well, there will not be any feature updates at all in the KTextEditor interfaces itself. Since Kate Part and the Kate Application are developed outside of kdelibs, Kate Part and Kate will most certainly see another feature release. Especially, since there are already a lot of improvements for the [vi input mode in the pipe][14] for Kate in KDE SC 4.12.

After that, we plan to port Kate to Qt5 and the new frameworks 5 libraries. We already added a lot of &#8220;KDE 5&#8221; todo-markers to the Kate source code, meaning that we will probably work on a 5 port rather sooner than later. We will address this schedule in a separate post later this year, so stay tuned!

 [1]: /2013/01/06/kate-in-kde-4-10/ "Kate in KDE 4.10"
 [2]: /2013/05/31/a-rich-python-console-and-more-in-kate-editor/ "Python plugins"
 [3]: /2013/02/18/new-plugins-to-the-kate-utils-to-python-javascript-django-and-xml/ "Python plugins"
 [4]: /2012/12/02/python-plugin-gets-support-for-python3/ "Python3 support"
 [5]: /join-us/ "Join Us"
 [6]: /kate-vi-mode/ "Kate vi input mode"
 [7]: /2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff
 [8]: /2013/09/06/kate-vim-progress/
 [9]: /2011/12/21/kate-in-kde-4-8/ "Kate in KDE 4.8"
 [10]: /2013/03/24/text-folding-reloaded/ "Kate Text Folding"
 [11]: /2013/03/27/new-text-folding-in-kate-git-master/ "Kate Text Folding"
 [12]: /2012/11/06/passive-notifications-in-kate-part/ "Introducing passive notifications"
 [13]: /2013/04/02/kate-search-replace-highlighting-in-kde-4-11/ "Search & replace in KDE 4.11"
 [14]: /?p=2727 "Vi Input Mode in KDE SC 4.12"