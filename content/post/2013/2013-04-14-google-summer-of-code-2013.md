---
title: Google Summer of Code 2013
author: Joseph Wenninger

date: 2013-04-14T09:04:30+00:00
url: /2013/04/14/google-summer-of-code-2013/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
tags:
  - planet

---
If someone is interested in doing some coding for Kate-Part, Kate, KTexteditor during this years  Summer of Code, just drop a proposal at the kwrite-devel mailing list for discussion.