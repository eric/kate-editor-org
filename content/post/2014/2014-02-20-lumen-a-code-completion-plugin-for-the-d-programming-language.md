---
title: Lumen – A Code-Completion Plugin for the D Programming Language
author: David Herberth

date: 2014-02-20T14:11:03+00:00
url: /2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/
categories:
  - Common
tags:
  - planet

---
<span style="line-height: 1.5;">I am </span><span style="line-height: 1.5;">the original author of the Lumen KTextEditor plugin and I am happy to announce, I just committed it to the Kate repository for KDE 4.13!</span>

Lumen is just the name for a plugin providing code-completion for th<span style="line-height: 1.5;">e D programming language in KTextEditor/Kate and KDevelop. But Lumen is just a connection between the editor and the </span><em style="line-height: 1.5;">D Completion Daemon (a server providing all the information)</em><span style="line-height: 1.5;"> called  </span><a style="line-height: 1.5;" title="DCD - D completion Daemon" href="https://github.com/Hackerpilot/DCD">DCD</a><span style="line-height: 1.5;">. The plugin currently supports all major features of the completion server: feeding the server with imp</span><span style="line-height: 1.5;">ort files, displaying documentation and several types of completion:</span>

**Imports:**

<p style="text-align: center;">
  <img class="size-full wp-image-3181 aligncenter" style="line-height: 1.5;" alt="example of lumen (imports)" src="/wp-content/uploads/2014/02/import01.png" width="381" height="189" srcset="/wp-content/uploads/2014/02/import01.png 381w, /wp-content/uploads/2014/02/import01-300x148.png 300w" sizes="(max-width: 381px) 100vw, 381px" />
</p>

**Basic Completion:**

<p style="text-align: center;">
  <a href="/wp-content/uploads/2014/02/completion01.png"><img class="size-full wp-image-3183 aligncenter" alt="example of lumen (code completion)" src="/wp-content/uploads/2014/02/completion01.png" width="310" height="335" srcset="/wp-content/uploads/2014/02/completion01.png 310w, /wp-content/uploads/2014/02/completion01-277x300.png 277w" sizes="(max-width: 310px) 100vw, 310px" /></a>
</p>

**Completion (overloaded Function):**

<p style="text-align: center;">
  <a href="/wp-content/uploads/2014/02/completion02.png"><img class="size-full wp-image-3184 aligncenter" alt="example of lumen (code completion)" src="/wp-content/uploads/2014/02/completion02.png" width="415" height="362" srcset="/wp-content/uploads/2014/02/completion02.png 415w, /wp-content/uploads/2014/02/completion02-300x261.png 300w" sizes="(max-width: 415px) 100vw, 415px" /></a>
</p>

**Calltips:**

<p style="text-align: center;">
  <a href="/wp-content/uploads/2014/02/calltip01.png"><img class=" wp-image-3182 aligncenter" alt="example of lumen (calltips)" src="/wp-content/uploads/2014/02/calltip01.png" width="509" height="259" srcset="/wp-content/uploads/2014/02/calltip01.png 509w, /wp-content/uploads/2014/02/calltip01-300x152.png 300w" sizes="(max-width: 509px) 100vw, 509px" /></a>
</p>

&nbsp;

To make Lumen work you have to install [DCD][1], unfortunately no Linux distribution has DCD packaged so far. Luckily the D community provides a remedy.

For all Debian/apt based distributions like Ubuntu and Debian of course, there is the [d-apt][2], simply follow the instructions on how to setup the apt-repository and install DCD via apt.  For Archlinux exists an [AUR Package][3]. Everyone else has to setup DCD manually, but it&#8217;s really not hard, simply follow [these instructions][4].

After installing DCD edit _~/.config/dcd/dcd.conf_ (create if it does not exist already) and add a path to your D include/import files (phobos), for me this is _/usr/include/dlang/dmd_ (on ArchLinux), for other installations this would most likely be _/usr/include/d_. Furthermore Lumen will try to read a _.lumenconfig_ in every parent folder of the currently opened D source file and add every line in this file as include path to the DCD server. Add all dependencies of your current project to this file.

Now start the completion server with _dcd-server_, enable the Lumen plugin in your settings and you will have code completion for the D programming language in your favorite editor!

 [1]: https://github.com/Hackerpilot/DCD
 [2]: http://d-apt.sourceforge.net/
 [3]: https://aur.archlinux.org/packages/dcd-git/
 [4]: https://github.com/Hackerpilot/DCD#setup