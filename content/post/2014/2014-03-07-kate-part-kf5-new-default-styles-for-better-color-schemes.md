---
title: 'Kate Part (KF5): New Default Styles for better Color Schemes'
author: Dominik Haumann

date: 2014-03-07T21:31:06+00:00
url: /2014/03/07/kate-part-kf5-new-default-styles-for-better-color-schemes/
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
Kate Part gained 17 new default styles in addition to the existing 14 default styles. These changes are available for Kate based on the KDE frameworks 5 initiative and currently live in <a title="KTextEditor git module" href="https://projects.kde.org/projects/frameworks/ktexteditor/repository" target="_blank">ktexteditor.git module</a>.

Default Styles are <span style="line-height: 1.5;">predefined font and color styles that are used by Kate Part&#8217;s syntax highlighting. For instance, Kate Part always had a default style for comments. Therewith, the comments in all syntax highlighting files look the same (by default, a gray color). Or keywords are by default always bold and black.</span>

However, during the last years it became apparent that the list of 14 default styles were not always enough. Consequently, lots of syntax highlighting files defined their own hard-coded colors. For instance, doxygen comments were hard-coded in a dark blue color. The problem with hard-coded colors is that they do not adapt when changing the default styles. Hence, doxygen comments were barely readable on a dark color scheme.

Therefore, a discussion took place on kwrite-devel (<a title="Discussion about Default Styles" href="http://lists.kde.org/?l=kwrite-devel&m=139293479701378&w=2" target="_blank">thread 1</a>, <a title="Discussion about Default Styles" href="http://lists.kde.org/?l=kwrite-devel&m=139271439016152&w=2" target="_blank">thread 2</a>, <a title="Final list of Default Styles" href="http://lists.kde.org/?l=kwrite-devel&m=139327713701324&w=2" target="_blank">thread 3</a>) that ended in 17 new default styles and a categorization of these as follows (the new default styles are bold):

Category &#8220;**Normal Text and Source Code**&#8221;

  * dsNormal: default for normal text and source code.
  * dsKeyword: Used for language keywords.
  * dsFunction: Used for function definitions and function calls.
  * **dsVariable**: Used for variables, if applicable. Example: $foo in PHP.
  * **dsControlFlow**: Used for control flow highlighting, e.g., if, then, else, return, continue.
  * **dsOperator**: Used for operators such as +, -, *, / and :: etc.
  * **dsBuiltIn**: Used for built-in language classes and functions, if applicable.
  * **dsExtension**: Used for extensions, such as Qt or boost.
  * **dsPreprocessor**: Used for preprocessor statements.
  * **dsAttribute**: Used for attributes of functions or objects, e.g. @override in Java, or \_\_declspec(&#8230;) and \_\_attribute__((&#8230;))in C/C++.

Category &#8220;**Strings & Characters**&#8221;

  * dsChar: Used for a single character.
  * **dsSpecialChar**: Used for an escaped character in strings, e.g. &#8220;hello\n&#8221;.
  * dsString: Default for strings.
  * **dsVerbatimString**: Used for verbatim strings such as HERE docs.
  * **dsSpecialString**: Used for special strings such as regular expressions in ECMAScript or LaTeX math mode.
  * **dsImport**: Used for includes, imports, modules, or LaTeX packages

Category &#8220;**Numbers, Types & Constants**&#8221;

  * dsDataType: Used for data types such as int, char, float etc.
  * dsDecVal: Used for decimal values.
  * dsBaseN: Used for numbers with base other than 10.
  * dsFloat: Used for floating point numbers.
  * **dsConstant**: Used for language constants, e.g. True, False, None in Python or nullptr in C/C++.

Category &#8220;**Comments & Documentation**&#8221;

  * dsComment: Used for normal comments.
  * **dsDocumentation**: Used for comments that reflect API documentation, e.g., the default style for /*\* \*/ comments.
  * **dsAnnotation**: Used for annotations in comments, e.g. @param in Doxygen or JavaDoc.
  * **dsCommentVar**: Used to refer to variables in a comment, e.g. after @param <identifier> in Doxygen or JavaDoc.
  * dsRegionMarker: Used for region markers, typically defined by BEGIN/END.
  * **dsInformation**: Used for information, e.g. the keyword @note in Doxygen.
  * **dsWarning**: Used for warnings, e.g. the keyword @warning in Doxygen.
  * dsAlert: Used for comment specials such as TODO and WARNING in comments.

Category &#8220;**Miscellaneous**&#8221;

  * dsOthers: Used for attributes that do not match any of the other default styles.
  * dsError: Used to indicate wrong syntax.

### Existing Syntax Highlighting Files

If the new default styles are not used, syntax highlighting files are backwards compatible to Kate Part in KDE SC 4. However, the plan is to use the new default styles where applicable to avoid hard-coded colors. To this end, the **kateversion** attribute in the **language** element will be set to 5.0 (yes, Kate Part&#8217;s version changed from 3 to 5 to match KDE frameworks 5) to avoid loading incompatible syntax highlighting xml files in older Kate Part versions. Example:

<pre>&lt;language name="C++" <strong>kateversion="5.0"</strong> [other attributes omitted]&gt;</pre>

With the new default styles, the Default Styles tab looks as follows:  
<a style="line-height: 1.5;" href="/wp-content/uploads/2014/03/default-styles-5.png"><img class="aligncenter size-full wp-image-3231" alt="Default Styles in KF5" src="/wp-content/uploads/2014/03/default-styles-5.png" width="994" height="1034" srcset="/wp-content/uploads/2014/03/default-styles-5.png 994w, /wp-content/uploads/2014/03/default-styles-5-288x300.png 288w, /wp-content/uploads/2014/03/default-styles-5-984x1024.png 984w" sizes="(max-width: 994px) 100vw, 994px" /></a>

&nbsp;

In comparison, the KDE 4.x version looks like this:  
[<img class="aligncenter size-full wp-image-3235" alt="Default Styles in KDE 4" src="/wp-content/uploads/2014/03/default-styles-4.png" width="923" height="615" srcset="/wp-content/uploads/2014/03/default-styles-4.png 923w, /wp-content/uploads/2014/03/default-styles-4-300x199.png 300w" sizes="(max-width: 923px) 100vw, 923px" />][1]

&nbsp;

The new default style colors are not fixed yet, so improvements and additional color themes we can ship with Kate Part by default are a welcome addition!

### A Note at 3rd Party Developers

There are other implementations (such as in <a title="Qt Creator's Kate Syntax Highlighting" href="http://doc.qt.digia.com/qtcreator-2.4/creator-highlighting.html" target="_blank">Qt Creator</a> or for <a title="Haskell Kate Syntax Highlighting" href="http://hackage.haskell.org/package/highlighting-kate" target="_blank">Haskell</a>). If you developers read this, we encourage you to add these default styles as well once Kate Part 5 is released (stable). Otherwise, the new syntax highlighting files may not be compatible.

 [1]: /wp-content/uploads/2014/03/default-styles-4.png