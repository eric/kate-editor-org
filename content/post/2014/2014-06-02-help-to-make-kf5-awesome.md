---
title: Help to make KF5 awesome!
author: Christoph Cullmann

date: 2014-06-02T16:46:01+00:00
url: /2014/06/02/help-to-make-kf5-awesome/
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
You like KDE software?

You like to have a polished and nice KDE Frameworks 5 release?

But you can&#8217;t help out yourself by coding, translating, bug finding, designing, documenting and whatever?

=> Feel free to fund our <a title="Fund Raising for Randa 2014" href="http://www.kde.org/fundraisers/randameetings2014/" target="_blank">Randa Meetings 2014</a>.

Perhaps it feels strange that a free/open source project wants money, but sprints & meetings are not for free, given you need to pay for the accommodations, travel, rooms for hacking, &#8230;

Therefore: Spread the word and help us to reach <a title="Fund Raising for Randa 2014" href="http://www.kde.org/fundraisers/randameetings2014/" target="_blank">our fundraising goal</a>!