---
title: Kate - LSP Client Status
author: Christoph Cullmann
date: 2020-01-01T19:51:00+02:00
url: /post/2020/2020-01-01-kate-lsp-client-status/
---

## Initial release with 19.12.0

The LSP client plugin for Kate made it into the [19.12 release](https://kde.org/announcements/releases/19.12/).

<p align="center">
    <a href="/post/2020/2020-01-01-kate-lsp-client-status/images/kate-lsp-client.png" target="_blank"><img width=500 src="/post/2020/2020-01-01-kate-lsp-client-status/images/kate-lsp-client.png"></a>
</p>

As this is the first official release of this plugin, it still has a lot of rough edges.

I use it at the office and it behaves well enough for my daily work on C/C++ and OCaml code.

## Multiple languages supported per default

We ship default configurations for a bunch of languages, out-of-the-box the stuff should work with:

* C/C++ via [clangd](https://clang.llvm.org/extra/clangd/)
* D via [dls](https://github.com/d-language-server/dls)
* Fortran via [fortran-language-server](https://github.com/hansec/fortran-language-server)
* Go via [go-langserver](https://github.com/sourcegraph/go-langserver)
* Latex/BibTeX via [texlab](https://texlab.netlify.com/)
* OCaml via [merlin](https://github.com/ocaml/merlin)
* Python via [pyls](https://github.com/palantir/python-language-server)
* Rust via [rls](https://github.com/rust-lang/rls)

You can configure additional servers via a JSON config file.

## Improved configuration for >= 19.12.1

Unfortunately, for the 19.12.0 release, we still had the issue that the mapping of syntax highlighting modes to LSP language names was more or less hard-coded inside the C++ sources.
This made it awkward to add other languages for which there is no 1:1 match of these two things (beside casing).

This is now rectified in master and backported to the 19.12 branch (should be in the next patch releases).

The mapping is now specified as a regular expression inside the JSON config that then is applied case-insensitive to the highlighting mode.

The shipped default config was adjusted to this, the current state in master & 19.12 is something like this:

{{< highlight json >}}
{
    "servers": {
        "bibtex": {
            "use": "latex",
            "highlightingModeRegex": "^BibTeX$"
        },
        "c": {
            "command": ["clangd", "-log=error", "--background-index"],
            "commandDebug": ["clangd", "-log=verbose", "--background-index"],
            "url": "https://clang.llvm.org/extra/clangd/",
            "highlightingModeRegex": "^(C|ANSI C89|Objective-C)$"
        },
        "cpp": {
            "use": "c",
            "highlightingModeRegex": "^(C\\+\\+|ISO C\\+\\+|Objective-C\\+\\+)$"
        },
        "d": {
            "command": ["dls", "--stdio"],
            "url": "https://github.com/d-language-server/dls",
            "highlightingModeRegex": "^D$"
        },
        "fortran": {
            "command": ["fortls"],
            "rootIndicationFileNames": [".fortls"],
            "url": "https://github.com/hansec/fortran-language-server",
            "highlightingModeRegex": "^Fortran.*$"
        },
        "latex": {
            "command": ["texlab"],
            "url": "https://texlab.netlify.com/",
            "highlightingModeRegex": "^LaTeX$"
        },
        "go": {
            "command": ["go-langserver"],
            "commandDebug": ["go-langserver", "-trace"],
            "url": "https://github.com/sourcegraph/go-langserver",
            "highlightingModeRegex": "^Go$"
        },
        "python": {
            "command": ["python3", "-m", "pyls", "--check-parent-process"],
            "url": "https://github.com/palantir/python-language-server",
            "highlightingModeRegex": "^Python$"
        },
        "rust": {
            "command": ["rls"],
            "rootIndicationFileNames": ["Cargo.lock", "Cargo.toml"],
            "url": "https://github.com/rust-lang/rls",
            "highlightingModeRegex": "^Rust$"
        },
        "ocaml": {
            "command": ["ocamlmerlin-lsp"],
            "url": "https://github.com/ocaml/merlin",
            "highlightingModeRegex": "^Objective Caml.*$"
        }
    }
}
{{< / highlight >}}

This should make it possible to add more languages easily as custom configuration without the need to patch the C++ sources to fix-up the mapping.

Sorry that we missed to make this possible in the initial release and thanks to the people that reported this issue to us!

In master, the documentation is updated to cover this, too.

In short: *highlightingModeRegex* specifies a regular expression that is applied to the highlighting mode to decide which LSP language to use.

## Contributions welcome!

As you can see in the above list, we still per default only cover less than 10 LSP servers.

I think we should aim to support as many as possible with the default configuration to make it easier for people to just use our stuff.

If you happen to have some local config for other languages, please contribute them back.

Just open a merge request [here](https://invent.kde.org/utilities/kate/merge_requests) at our [KDE GitLab instance](https://invent.kde.org).

Beside adding more default configured languages, contributions to any other part of the plugin are welcome, too.

Milian Wolff contributed initial support for [semantic highlighting](https://invent.kde.org/utilities/kate/merge_requests/47) to the master branch.

Below the same part of *katedocument.cpp* as in the initial screenshot above, but with the semantic highlighting of master turned on.

<p align="center">
    <a href="/post/2020/2020-01-01-kate-lsp-client-status/images/kate-lsp-semantic-highlighting.png" target="_blank"><img width=500 src="/post/2020/2020-01-01-kate-lsp-client-status/images/kate-lsp-semantic-highlighting.png"></a>
</p>

This is still default off and needs a lot of improvements to be more efficient & properly fitting into the color schemes.
But it is a good step into the right direction on can further improve on.

Beside, this merge request shows that it is relatively straight forward to add new features to this plugin, it is no rocket science, at least not for small enhancements.

An other interesting thing will be how well it works on Windows.
I must confess, I didn't try it there as I mostly develop on Unices.
As soon as the first 19.12 release builds are rolling out there, I hope we get feedback & help on that operating system, too.
Given we now seem to have a [growing Windows userbase](/post/2019/2019-12-26-windows-store-status/), perhaps somebody steps up to take care of that!

## Let 2020 be a successful year for our LSP client

I hope a lot of people use the new plugin in the new year and we get more people that contribute to this effort.

I think having a well working LSP integration will lift Kate from a mere text editor to a lightweight IDE for a wide range of languages.

At least for me, this is the most trilling new Kate feature of the last year, as it really enhances my daily workflow.

Thanks already to Mark for contributing most of the code of this plugin and all people that helped to improve it.

And a happy new year for all of you reading this!

### Discussion

If you want to chime in on this, feel free to join the discussion at the [KDE reddit](https://www.reddit.com/r/kde/comments/eilzt2/kate_lsp_client_status/).

If you want to contribute, just join us at [invent.kde.org](https://invent.kde.org/utilities/kate) or on [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org).
