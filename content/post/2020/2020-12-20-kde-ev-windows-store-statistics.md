---
title: KDE e.V. Windows Store Statistics
author: Christoph Cullmann
date: 2020-12-20T15:15:00+02:00
url: /post/2020/2020-12-20-kde-ev-windows-store-statistics/
---

The end of 2020 is nigh, let's take a look at the current statistics of all applications in the Windows Store published with the KDE e.V. account.

Since the last report in [August](/post/2020/2020-08-11-windows-store-monthly-statistics/) we have a newcomer: [LabPlot](https://labplot.kde.org/).

### Last 30 days statistics

Here are the number of acquisitions for the last 30 days (roughly equal to the number of installations, not mere downloads) for our applications:

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 5,293 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 4,886 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 1,433 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 710 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 400 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 228 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 116 acquisitions

A nice stream of new users for our software on the Windows platform.

### Overall statistics

The overall acquisitions since the applications are in the store:

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 74,438 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 69,008 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 15,008 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 8,627 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 4,008 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 3,289 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 134 acquisitions

### Help us!

If you want to help to bring more stuff KDE develops on Windows, we have some meta [Phabricator task](https://phabricator.kde.org/T9575) were you can show up and tell for which parts you want to do work on.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

Thanks to all the people that help out with submissions & updates & fixes!

If you encounter issues on Windows and are a developer that wants to help out, all KDE projects really appreciate patches for Windows related issues.

Just contact the developer team of the corresponding application and help us to make the experience better on any operating system.

### ;=) More to come, I hope...

Let's see how this progresses in the next years, I think 2020 was a good year for our applications on Windows.

I hope we will see more applications making their way to the store in the future.

And let's see if more people step up to help with fixing remaining issues for Windows, too!
