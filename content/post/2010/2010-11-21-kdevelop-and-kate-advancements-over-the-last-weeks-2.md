---
title: KDevelop and Kate advancements over the last weeks
author: Milian Wolff

date: 2010-11-21T15:38:15+00:00
excerpt: |
  Hey all,
  
  I didn&#8217;t blog in a long while so I thought I&#8217;d dump some notable things I did over the last weeks. It&#8217;s probably neither complete nor thorough - you should try it all out to see it for yourself :)
  
  Kate
  
  Lets start small wit...
url: /2010/11/21/kdevelop-and-kate-advancements-over-the-last-weeks-2/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/kdevelop-and-kate-advancements-over-the-last-weeks#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/154
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kdevelop-and-kate-advancements-over-the-last-weeks
syndication_item_hash:
  - c76ee7f37f4327b075bdab092160ecd2
categories:
  - KDE

---
Hey all,

I didn&#8217;t blog in a long while so I thought I&#8217;d dump some notable things I did over the last weeks. It&#8217;s probably neither complete nor thorough - you should try it all out to see it for yourself :)

##### Kate

Lets start small with my Kate contributions. I really concentrate on KDevelop nowadays since Kate works quite well for me and I don&#8217;t have many itches to scratch anymore :) So, what did I do these last weeks? Mostly cleanup and performance work, especially regarding MovingRanges which of course is mostly needed for a good experience in the upcoming KDevelop 4.2. With the help of Dominik Haumann I also went through our indentation unit tests and made sure we expect all currently failing test cases, as well as fixing those I could. Now we (finally!) have a passing test suite and get noticed about introduced regressions. Awesome!

##### KDevelop

Of course I still concentrate my contributions on KDevelop and related plugins. What I find quite interesting is that I&#8217;m now for over two months in a _&#8220;polish-and-bugfixing&#8221;_-mode, without adding much new features. I think that shows that I&#8217;m quite satisfied with the existing features, just not with the way they are integrated. E.g. the Kate performance improvements above were done in order to make &#8220;reformat source&#8221; not hang the IDE for a couple of seconds anymore, in KDevelop 4.2 it will only be like one second for _really_ big source files. I&#8217;ve also tackled our black sheep, the &#8220;launch configuration&#8221; dialog. I didn&#8217;t rewrite it, but I did some polishing and have some more ideas to hopefully get it into a useable state. Oh, I just remember: I also spent some time on the &#8220;Snippets&#8221; plugin, restructured the editor dialogs a bit and made them use Katepart as well, for syntax highlighting and proper editor features :)

I generally care about small things nowadays, like e.g. that all widgets have a proper tooltip. This is also something where you all could help: If you find a widget in KDevelop master (4.2) that has no tooltip, please notify me! I&#8217;ve added those e.g. to the outline and quickopen toolbar widgets among others.

And since KDevelop 4.1 was in beta stage I&#8217;ve fixed lots of bugs and continue to do that. I triage each new bug for KDevelop/KDevplatform, request feedback and valgrind logs and try to fix whatever I can. Even now I can safely assume that KDevelop 4.2 will be more stable than 4.1, even though we did lots of architectural changes in the platform, esp. regarding MovingRanges.

And now to the last and to me most pleasing part: New features! Remember how I said I didn&#8217;t do lots of feature development at all these past days? This is basically true, instead I let others do the work :) We really get more and more patches and merge requests by other people, and I did merge quite some changes already. We have an [improved &#8220;find in files&#8221; plugin][1] now, and [the French students][2] are working hard on giving it replace functionality as well! We already merged their [improved QtHelp plugin][3] that gives you the ability to integrate arbitrary <span class="geshifilter"><code class="text geshifilter-text">.qch</code></span> files into KDevelop, e.g. [the KDE Api documentation][4]. They also work on [<span class="geshifilter"><code class="text geshifilter-text">man</code></span> integration](, which will be a joy for all STD C / C++ developers.

Furthermore we now have a [&#8220;Filter&#8230;&#8221; line edit][5] above the project manager view, [&#8220;Argument Dependent Lookup&#8221;][6] for the C++ plugin, a [much improved &#8220;Problems&#8221; toolview][7], with the ability to show TODOs etc. pp.

This is just from the last few weeks, and it&#8217;s work done by many new contributors. It&#8217;s really very cool to see this happening.

Oh, and I personally worked a bit on the [QMake support][8] for KDevelop, I&#8217;m not sure whether I will manage to get this into a usable state for KDevelop 4.2, but I already use it at work.

Which brings me to the last part of this blog post, release schedules! I&#8217;ll finally release a KDevelop 4.1.1 bug fix version next week, and will make sure that there is a 4.2 release just before / shortly after the KDE 4.6 release in January. The big &#8220;issue&#8221; there is that **KDevelop 4.1 will not work with KDE 4.6+**, since there are no SmartRanges in Kate from 4.6 anymore. So a big fat warning to any adventurous tester out there: If you want to try KDE 4.6 betas, remember that you&#8217;ll have to get KDevelop master (== 4.2) as well somehow!

So, that&#8217;s it for now, have a nice sunday everyone :)

 [1]: http://gitorious.org/kdevelop/kdevplatform/merge_requests/33
 [2]: http://blog.ben2367.fr/tag/kdevelop/
 [3]: http://blog.ben2367.fr/2010/11/11/kdevelop-when-development-documentation-come-to-you/
 [4]: http://api.kde.org/
 [5]: http://gitorious.org/kdevelop/kdevplatform/merge_requests/40
 [6]: http://gitorious.org/kdevelop/kdevelop/merge_requests/27
 [7]: http://gitorious.org/kdevelop/kdevplatform/merge_requests/39
 [8]: http://websvn.kde.org/trunk/playground/devtools/kdevelop4-extra-plugins/qmake/