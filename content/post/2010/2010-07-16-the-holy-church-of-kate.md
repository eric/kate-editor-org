---
title: The Holy Church of Kate!
author: Dominik Haumann

date: 2010-07-16T20:51:31+00:00
url: /2010/07/16/the-holy-church-of-kate/
categories:
  - Events
tags:
  - planet

---
Last year we already discovered that <a title="News from the Holy Kate Land" href="/2009/09/18/news-from-the-holy-kate-land/" target="_self">Kate is truly the only holy text editor</a>* (again thanks to rms for making us aware of it)! (and btw, <a title="xkcd" href="http://xkcd.com/378/" target="_blank">xkcd is totally wrong</a>). And now we even have yet another proof: The holy Kate maintainer and the naked woman! <a title="Join Us!" href="/join-us/" target="_self">Enjoy</a> :-)

<p style="text-align: center;">
  <img class="size-full wp-image-420 aligncenter" title="naked-woman" src="/wp-content/uploads/2010/07/naked-woman.jpg" alt="The Kate Maintainer and The Naked Woman" width="600" height="800" srcset="/wp-content/uploads/2010/07/naked-woman.jpg 600w, /wp-content/uploads/2010/07/naked-woman-225x300.jpg 225w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

<p style="text-align: center;">
  (*) this is an insider from the joint KDE and Gnome conference (gcds) in Las Palmas, Gran Canaria, 2009.
</p>