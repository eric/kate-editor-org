---
title: New kate-editor.org launched
author: Christoph Cullmann

date: 2010-07-09T09:23:39+00:00
url: /2010/07/09/new-kate-editor-org-launched/
categories:
  - Common
tags:
  - planet

---
After some years of no real updates, beside the brave work of Dominik, a new [kate-editor.org][1] is launched.

It will be used to aggregate the blogs of the Kate developers and provide information about current development in Kate/KWrite/KatePart and guides for users.

Hope more people join the effort to once again provide a constant stream of up-to-date information.

 [1]: /