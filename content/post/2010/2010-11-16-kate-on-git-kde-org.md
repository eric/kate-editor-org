---
title: Kate on git.kde.org
author: Christoph Cullmann

date: 2010-11-16T19:32:47+00:00
url: /2010/11/16/kate-on-git-kde-org/
categories:
  - Common
tags:
  - planet

---
Goodbye to Gitorious, welcome to git.kde.org.

You can find the Kate project [here][1].  
The [&#8220;Get It!&#8221;][2] page on the homepage is updated, too.  
Push access is still restricted, but that will not last for ever ;)

Thanks to the sysadmin team of KDE, thanks guys, for all the work you put into the new infrastructure (and to help me with my faults ;)  
And thanks to Gitorious, it was a nice time there, and I learned to appreciate Git, like many Kate developers (some appreciated it even before).

Like before, all changes in SVN and Git will be synced by me, as Kate is still released with KDE SC from SVN.

For the future I would like (and most kate devs, too) to develop solely in the Kate repository and have Kate released from there with KDE SC (instead spread over several modules).  
But this must still be discussed with the release team after 4.6.  
Beside, this means no split off from KDE, but it makes contributing to Kate that much easier, given atm you would need to checkout/clone kdelibs + kdebase + kdesdk and try to build there the Kate relevant parts, which is just no fun, if you just want to hack on some small bug or feature :/

 [1]: https://projects.kde.org/projects/svn-bridge/kate
 [2]: /get-it/