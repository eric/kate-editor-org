---
title: Snippets In KDevelop / Kate
author: Milian Wolff

date: 2010-02-03T15:59:46+00:00
excerpt: |
  Hey all!
  
  Just wanted to give you a little rundown on Snippets in Kate 4.4 (via the snippets_tng plugin) and KDevelop Beta 8 (soon to be released).
  
  Note: The Kate plugin was written by Jowenn and introduced me to all these nice features. For KDevelop ...
url: /2010/02/03/snippets-in-kdevelop-kate/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/snippets-in-kdevelop-kate#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/119
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/snippets-in-kdevelop-kate
syndication_item_hash:
  - 4ec343ef2dc6369b33ac651565db11d9
  - 073ad957a3222a40d0eb9b46886f01d4
categories:
  - KDE

---
Hey all!

Just wanted to give you a little rundown on Snippets in Kate 4.4 (via the snippets_tng plugin) and KDevelop Beta 8 (soon to be released).

Note: The Kate plugin was written by Jowenn and introduced me to all these nice features. For KDevelop I wrote a somewhat simpler yet imo better implementation. We will try to get the best of both worlds into KDE 4.5. Stay tuned!

##### General Usage & Features

  * create a snippet repository (or download via GHNS [see below])
  * create snippets in that repository
  * insert snippets via the snippets view (i.e. double click), or (imo better/faster) insert them via code-completion (remember: CTRL + Space requests code completion at the current cursor position!).
  * snippet gets inserted (properly indented) and potential placeholders/variables get expanded. A variable is something like <span class="geshifilter"><code class="text geshifilter-text">%{date}</code></span> or <span class="geshifilter"><code class="text geshifilter-text">${email}</code></span>. Also take a look at the [API documentation][1].
  * variables that get inserted via &#8220;${&#8230;}&#8221; will be &#8220;selectable&#8221;, meaning you can jump from one var to the other by hitting TAB / Shift TAB
  * the <span class="geshifilter"><code class="text geshifilter-text">%{...}</code></span> vars will only get expanded and inserted, without getting selectable.
  * multiple occurrences of the same variable will be updated once one of them gets edited, something that is called &#8220;mirroring&#8221; in other editors.
  * once one edits ESC the cursor is placed at the end of the snippet or to the first occurrence of <span class="geshifilter"><code class="text geshifilter-text">${cursor}</code></span> or <span class="geshifilter"><code class="text geshifilter-text">%{cursor}</code></span> and the user types something, the snippet-handler quits and you are left with your normal editor until you insert the next snippet
  * nested snippets (i.e. insert snippet than insert another one) should &#8220;just work&#8221;.

##### Snippet Management

  * group snippets by file type, i.e. PHP snippets will only be offered during code completion when one edits a PHP file.  
    Note: In KDevelop and KDE 4.4 nested documents are supported, e.g. create a CSS snippet and it will be shown inside the CSS parts of a HTML document or similar. This uses my HighlightInterface I wrote for KDE 4.4. I still have to rewrite some parts of the snippets_tng plugin for Kate so that it works there as well
  * group snippets in repositories, set an Author and a License of your choice
  * publish snippet repositories via GHNS: In Kate you can already download snippets from GHNS but we sadly don&#8217;t have any repos up on opendesktop&#8230; I&#8217;ll have to add some prior to KDE 4.4. Also we didn&#8217;t have enough time to implement uploading of Repos from inside Kate in time for KDE 4.4. So stay tuned for KDE 4.5. KDevelop currently has no support for GHNS, but I plan to fix this tomorrow or the next days - together with uploading from inside KDevelop, i.e. all the nice features of GHNS v3.
  * in KDevelop (and someday in Kate as well) you can simply select a part of your currently opened document and select the &#8220;create snippet from selection&#8221; in the ContextMenu - easy & fast!

##### TODO

There&#8217;s much to do.

  * Highest priority right now for me is to get GHNS with all bells and whistles supported for KDevelop.
  * Then I&#8217;ll merge and integrate the Kate & KDevelop plugins as much as possible, so we have a reduce code base.
  * Also important is to make all shortcuts configurable
  * Another thing is: How could we improve interoperability even between e.g. editors like TextMate or Gedit? Both have snippets features and their bundles are available in the net. If we can support those we&#8217;d save us a lot of work

Also, I should probably do a screencast&#8230; Not now though ;-)

 [1]: http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1TemplateInterface.html