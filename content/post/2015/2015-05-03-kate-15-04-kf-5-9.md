---
title: Kate from KDE Applications 15.04 – KF 5.9
author: Christoph Cullmann

date: 2015-05-03T16:29:19+00:00
url: /2015/05/03/kate-15-04-kf-5-9/
categories:
  - KDE
  - Users
tags:
  - planet

---
I reinstalled my home machine last week with openSUSE 13.2 and installed their latest 15.04 packages from the KDE repos ;=)

That is the first time that I use a distro-shipped Kate that is based on KF5 (and no other Kate 4.x is installed any more as escape route).

I think I already have seen 1-2 glitches (like not needed questions for reload and some repaint issue on reload), still, overall, I am happy with what is shipped as 15.04.

Thanks to all people contributing to KF5 and Kate 5.x! Well done!  
And thanks to the packagers of 15.04, too. Nice to have up-to-date applications with just an additional repository to add!