---
title: Kate on Mac, HiDPI
author: Christoph Cullmann

date: 2015-10-14T20:48:32+00:00
url: /2015/10/14/kate-on-mac-hidpi/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Kate runs ;=)

[<img class="aligncenter size-medium wp-image-3672" src="/wp-content/uploads/2015/10/kate-300x245.png" alt="Kate on Mac HiDPI" width="300" height="245" srcset="/wp-content/uploads/2015/10/kate-300x245.png 300w, /wp-content/uploads/2015/10/kate-1024x836.png 1024w, /wp-content/uploads/2015/10/kate.png 1504w" sizes="(max-width: 300px) 100vw, 300px" />][1]

But as you can see, we have no icons, but that is as no work was invested into it and not even any icons are installed ;=)

I will maintain a guide how to build it on mac in the <a href="https://quickgit.kde.org/?p=kate.git&a=blob&f=mac.txt" target="_blank">kate.git/mac.txt</a> file. At the moment, this is all not that nice. Kate is able to open files via file dialog and it renders OK, modulo some glitches.

Still a long way to go, ATM the application bundle contains just the application binary + plist + icons. The libraries are still found only because of they are were they were compiled & installed. Plugins are not found either, need to take a look if that is just missing env vars or more.

At least, this is far better than at the beginning of the year, were all I had was instant segfaults.

Kate runs here btw. without any dbus server running, which means file opening in that window via command line won&#8217;t work, but still, compared to KWrite, which crashs on open dialog because of dbus missing, that is nice ;=)

 [1]: /wp-content/uploads/2015/10/kate.png