---
title: KWrite on Mac – Application Bundle
author: Christoph Cullmann

date: 2015-10-19T13:11:32+00:00
url: /2015/10/19/kwrite-on-mac-application-bundle/
categories:
  - Developers
  - KDE
tags:
  - planet

---
Hi,

after one week of patching frameworks (and KWrite/Kate), the first success can be seen: a kind of working application bundle for KWrite.

Still no icons, need to set icon theme + search paths right and bundle that, too, but need to investigate more how the icon lookup works.

[<img class="aligncenter size-medium wp-image-3680" src="/wp-content/uploads/2015/10/Bildschirmfoto-2015-10-19-um-15.00.00-300x245.png" alt="KWrite on Mac" width="300" height="245" srcset="/wp-content/uploads/2015/10/Bildschirmfoto-2015-10-19-um-15.00.00-300x245.png 300w, /wp-content/uploads/2015/10/Bildschirmfoto-2015-10-19-um-15.00.00-1024x836.png 1024w, /wp-content/uploads/2015/10/Bildschirmfoto-2015-10-19-um-15.00.00.png 1504w" sizes="(max-width: 300px) 100vw, 300px" />][1]

You can build the stuff on your own just following <a href="https://quickgit.kde.org/?p=kate.git&a=blob&f=mac.txt" target="_blank">kate.git/mac.txt</a> and calling once the macdeployqt tool afterwards on the /Applications/KDE/kwrite.app bundle (with -dmg to get the disk image file).

For Kate, the macdeployqt still misses an additional switch to deploy own plugins in addition, will add that in the next days, I hope. Have already some local hack, but want to have some patch I can hand in to Qt Company.

Here a link to the version you see above in the screenshot (not tested below Mac OS 10.10):

/download/kwrite-alpha-no-icons.dmg

A colleague tested it for me on a Mac without any dbus/macports/homebrew/co. stuff installed and it seems to work and not eat all his data ;=) But this is <span style="color: #ff0000;"><strong>ALPHA quality</strong></span>,  this means all things can happen, I didn&#8217;t test it that much beside using it for my last kate.git commit and starting it up for that screenshot above, not more ;=)

The good things: This stuff needs no patched Qt nor any patches to any KF5 stuff, this uses plain Qt 5.5.0 from the Qt Company installer and all modifications for frameworks are inside our master branch of framworks already.

The bad things: e.g. KIO doesn&#8217;t work atm (even if the io slaves would be in the bundle), therefore only local file support.

 [1]: /wp-content/uploads/2015/10/Bildschirmfoto-2015-10-19-um-15.00.00.png