---
title: Kate/KDevelop Sprint 2015 – Summary for Kate
author: Christoph Cullmann

date: 2015-10-11T13:49:06+00:00
url: /2015/10/11/katekdevelop-sprint-2015-summary-for-kate/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
The end of the joint Kate/KDevelop Sprint 2015 is coming closer, half of the people already have left to get home.

It was a good thing to have this sprint sponsored by the KDE e.V. and organized by Milian Wolff directly after the Qt World Summit 2015 here in Berlin. I think that made it easier for people to attend both events, like myself ;=)

I came to the sprint to work on getting our bugs sorted and fixed and to improve the state of the Mac port. I must confess, the bug fixing session worked out well, but I didn&#8217;t take a shot at Mac at all.

What did I achieve? I fixed many small bugs and I reintroduced the automatic brackets completion removed by accident in the KDE 4.x => KF5 transition.

In addition I cleaned up old things in our bugzilla. The new policy for Kate will be: wishes that not got any attention since 2 years will be closed, it makes no sense to keep them around for ever and it will only make overview about what is really wanted impossible.

In addition, I tried to help Kåre Särs to get Kate running on Windows. We want to have a way to build Kate with a unpatched Qt. At the moment that is not really feasible, as things like ui files are not found, xml syntax files are not found and so on. I started to solve that issues and at least KTextEditor framework itself should now be more or less Windows (and therefore Mac) compatible. But the other frameworks will still need love, like KXmlGui not being able to locate its own ui_standards.rc. Kate application is now compilable without any strange patches, too, on Windows.

Mandatory screenshot (be aware, to get menus and stuff working, we here randomly copied all files into the right user local directories):

[<img class="aligncenter size-medium wp-image-3647" src="/wp-content/uploads/2015/10/kate-on-windows1-300x239.png" alt="Kate on Windows" width="300" height="239" srcset="/wp-content/uploads/2015/10/kate-on-windows1-300x239.png 300w, /wp-content/uploads/2015/10/kate-on-windows1.png 840w" sizes="(max-width: 300px) 100vw, 300px" />][1]

Dominik Haumann did help us in our bug fixing effort, too. At the end of the sprint he started the painful work to review and fix the use of hard-coded colors in our highlighting files, work nobody really wants to do :=)

Sven Brauch helped to improve my reintroduced bracket completion to be not completely dumb .P

All in all, I think it was a productive sprint. Not that we all reached our goals, but we did make progress.

I think Kate & KTextEditor will be in a very good shape for the 15.16 KF5 Frameworks  and the next KDE Applications release. In addition you always get again momentum to work on more stuff in the close future by meeting up in person and do some socializing aka eating ;)

Therefore, to wrap it up: Nice work all guys, thanks by the Kate maintainer to all people that joint the sprint and special thanks to Milian for setting it up and the KDE e.V. for sponsoring it ;=)

P.S. Yeah, sponsoring implies that the e.V. has money for that. To keep such spring sponsoring possible in the future => <a href="https://kde.org/community/donations/" target="_blank">give us money</a>!

 [1]: /wp-content/uploads/2015/10/kate-on-windows1.png
