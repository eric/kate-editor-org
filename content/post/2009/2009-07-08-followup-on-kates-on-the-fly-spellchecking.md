---
title: Followup on Kate’s on-the-fly spellchecking
author: Dominik Haumann

date: 2009-07-08T10:09:00+00:00
url: /2009/07/08/followup-on-kates-on-the-fly-spellchecking/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/07/followup-on-kates-on-fly-spellchecking.html
categories:
  - Users

---
As there was a lot of feedback in the <a href="/2009/07/07/on-the-fly-spellchecking-in-kate/" target="_self">last blog</a>, I&#8217;ll answer some of the questions here.

> <span style="font-style: italic;">Where can I get this patch?</span>  
The relevant subversion revisions are r992778, r992779, r992780, r992784.

> <span style="font-style: italic;">Will it be available in 4.3, or only in 4.4?</span>  
As KDE 4.3 will be released end of the month, this feature will be available in KDE 4.4 and not earlier.

> <span style="font-style: italic;">Please, please tell me that it&#8217;s powered by Sonnet, one of the most awaited KDE4 pillar by me&#8230;</span>  
Yes, it uses Sonnet :)  
The old spellcheck dialog however still uses the old spellchecking code without Sonnet. Any volunteers to port this? Also, the on-the-fly spellchecking needs to be more configurable in Kate&#8217;s config dialog, e.g. selecting the correct language.

> <span style="font-style: italic;">Thanks so much, this was the feature I was mostly longing for in kate.</span>  
Yes, it is one of the [oldest reports][1] with <span style="font-weight: bold;">1245 votes</span>!

> What languages are supported support?  
On-the-fly spellchecking works if the specific itemDatas are marked with spellChecking=&#8221;true&#8221; or &#8220;1&#8221; in the xml highlighting definition files. UPDATE: On-the-fly spellchecking is enabled for all itemDatas by default in the xml highlighting defintion files. To disable it, you have to add <span style="font-weight: bold;">spellChecking=&#8221;false&#8221;</span> or &#8220;0&#8221; to an itemData, e.g. at the end of the [cpp.xml][2] file:

<pre>&lt;itemDatas&gt;
&lt;itemData name="Normal Text" <span style="font-weight: bold;">spellChecking="0"</span> /&gt;
...
&lt;itemData name="String" /&gt;
&lt;itemData name="String Char" <span style="font-weight: bold;">spellChecking="0"</span> /&gt;
&lt;itemData name="Comment" /&gt;
&lt;itemData name="Symbol" <span style="font-weight: bold;">spellChecking="0"</span> /&gt;
...
&lt;/itemData&gt;</pre>

So we have to go through all .xml files and change the relevant parts in the itemDatas section. And that&#8217;s where we need your help, as we don&#8217;t know all the languages :) &#8230;and if you want to test this feature, you first have to enable it in Tools > On-the-fly spellchecking.  
PS: Is there a better name? Maybe Inline spellchecking? Any linguistic experts around? :)

 [1]: http://bugs.kde.org/show_bug.cgi?id=33857
 [2]: http://websvn.kde.org/trunk/KDE/kdelibs/kate/syntax/data/cpp.xml?view=markup