---
title: first experience with Archlinux
author: Milian Wolff

date: 2009-10-18T00:24:14+00:00
excerpt: 'So, I kinda messed up my desktop right after the upgrade to karmic, because I was too greedy for performance and converted my root file system to ext4. Well, that worked like a charm on my laptop, but it broke my desktop. This is in no way karmic&#8217...'
url: /2009/10/18/first-experience-with-archlinux/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
"rss:comments":
  - 'http://milianw.de/blog/first-experience-with-archlinux#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/106
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/first-experience-with-archlinux
syndication_item_hash:
  - 077a2d7f7b2aa6cbec4a03fd95c8c22d
categories:
  - KDE

---
So, I kinda messed up my desktop right after the upgrade to karmic, because I was too greedy for performance and converted my root file system to ext4. Well, that worked like a charm on my laptop, but it broke my desktop. This is in no way karmic&#8217;s fault, it&#8217;s my own misbehavior. Thankfully I could rescue most of my data.

Since I&#8217;d had to reinstall anyways, I decided to finally try out Archlinux. I find the rolling release mantra very intriguing. Together with a &#8220;simpler&#8221; packaging, namely no splitting between <span class="geshifilter"><code class="text geshifilter-text">-dev</code></span> and <span class="geshifilter"><code class="text geshifilter-text">-dbg</code></span> packages like debian/ubuntu does, this is destined to be a good environment for a developer. I always hated it to track down missing <span class="geshifilter"><code class="text geshifilter-text">-dev</code></span> packages when compiling software. And don&#8217;t get me started on outdated software in repos&#8230; I just compiled kdelibs and the only missing build dependency was hspell, that I don&#8217;t need anyways. Under Jaunty I had to compile stuff from kdesupport to fulfill updated dependencies. And the list of not-found optional dependencies was huge, since I did not spent time to install all those <span class="geshifilter"><code class="text geshifilter-text">-dev</code></span> packages by hand&#8230;

My first impression of Archlinux is very good so far. I also finally migrated to 64bit wich works like a charm, no issues with flash or anything. Since I never used a 64bit Ubuntu/Debian I&#8217;m not sure, whether the perceived performance increase is due to the switch to 64bit or whether Archlinux optimized packages are responsible. Probably both. Nevertheless I can safely say that my system feels snappier than before.

Of course, the installation and initial setup is not as straight forward / easy as with Debian/Ubuntu: Yet it&#8217;s no big deal for anyone with some Linux experience. And, once everything is setup, you are running KDE again, so no real difference. Thanks to the Chakra team for kdemod, it works like a charm!

I might have spent a bit more time during the installation / initial configuration, but I think this would have happened also if I&#8217;d installed any other distro I&#8217;ve never used before, like OpenSuse or Fedora.

Oh and since I can install <span class="geshifilter"><code class="text geshifilter-text">sudo</code></span> I can keep my old habits. Neat.

The only thing I miss so far is <span class="geshifilter"><code class="text geshifilter-text">aptitude</code></span> with it&#8217;s straight forward command structure. Yaourt/Pacman is fast and nice, esp. with pacman-color, but the commands don&#8217;t feel as straight forward to me&#8230; Personal preference I&#8217;d say.

To conclude: Archlinux is very nice, I can wholeheartedly recommend using it so far. Probably nothing for a novice Linux user, yet perfect for advanced users. Very good as a development environment. Fast. Up to date. I like it :)

Now I can finally continue hacking on Kate/Kdelibs again :) I&#8217;m currently in the process of refactoring Kate&#8217;s implementation of the [TemplateInterface][1]. Even in it&#8217;s current state it already implements features like mirrored snippets and the like. But once I&#8217;ve finished with the cleanup I will try to implement some more of the features that are found in e.g. [yasnippet][2] for Emacs. I really wonder why nobody else did that already&#8230;

Once this is finished, you can expect that I will deeply integrate that feature in various places in KDevelop, especially for code completion, snippet plugin etc. pp. Stay tuned!

 [1]: http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1TemplateInterface.html
 [2]: http://code.google.com/p/yasnippet/