---
title: News from the Holy Kate Land
author: Dominik Haumann

date: 2009-09-17T22:36:00+00:00
url: /2009/09/18/news-from-the-holy-kate-land/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/09/news-from-holy-kate-land.html
categories:
  - Developers
  - Users

---
Since we now all know that Kate is holy (thanks to rms. By accident, he obviously confused Kate with emacs, though) let&#8217;s have a look at what&#8217;s going on. In the last months Kate development is quite active, so here is a quick update:

  * new: [on-the-fly spell checking][1] thanks to Michel Ludwig. Highlights include e.g. spell checking in comments of source code or latex parts. Also, constructs like sch\&#8221;on work in latex.
  * extended scripting support in the command line, more on that later
  * more and more mature vi input mode
  * [lots of bug fixing][2]. quite impressive bug squashing by Pascal Létourneau for more than 4 months now
  * lots of refactoring and code cleanups thanks to Bernhard!
  * &#8220;Find in Files&#8221; appears by default again in the tool view,
  * &#8220;File Browser&#8221; uses UrlNavigator, huge code cleanup
  * convenience updates of syntax highlighting
  * delayed highlighting of code folding ranges to prevent flickering on mouse move
  * new command line commands: &#8216;toggle-header&#8217; in the Open Header plugin. &#8216;grep&#8217; and &#8216;find-in-files&#8217;
  * haskell and lilypond indenter
  * much, much more, see commits for details.

Thanks to all contributors involved in Kate development. Keep it up :)

 [1]: http://dhaumann.blogspot.com/2009/07/followup-on-kates-on-fly-spellchecking.html
 [2]: https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=CLOSED&resolution=FIXED&resolution=WORKSFORME&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2009-06-25&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=