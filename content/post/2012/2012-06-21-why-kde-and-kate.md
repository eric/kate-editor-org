---
title: Why KDE, and Kate
author: shaheed

date: 2012-06-20T23:49:10+00:00
url: /2012/06/21/why-kde-and-kate/
pw_single_layout:
  - "1"
categories:
  - Common

---
I&#8217;ve been using and contributing to KDE on-and-off for a while, but our friends over in Gnome land were already busy by the time I got into the game. So why did I conceptually commit to KDE, and thence Kate? Consistency.

I grew up on VAX/VMS where it was possible &#8211; and even easy &#8211; to mix Ada code with C and Pascal, mix in a CLI that used the standard parser, generate error messages that looked and \*were\* like every other message in the system. Everything felt integrated.

And the editors? Ah yes, the editors. VAX EDT was where I started, and I \*loved\* the fact that I used the same editor whether I was writing a VAXmail, posting a VAXnote, or editing a file. So it was a bit of a dilemma when I encountered VAX Emacs, with all its seductive extensibility. I ended up using Emacs for serious editing, and EDT for everything else&#8230;that was hateful. Eventually, TPU and its precocious offspring LSE restored the status ante, and my sanity.

Scroll forward a few years, past Windows NT 3.5 and Visual Studio where I got hooked on GUIs, and we get to Qt and KDE libs. Even though there were several KDE text editors in those early days, it was a reasonable bet we&#8217;d rationalise, and I&#8217;m delighted to say we did (even when Kmail&#8217;s default isn&#8217;t KWrite &#8211; at least the look&#8217;n&#8217;feel was the same). I even went round making every Find and Replace dialog throughout KDE the same.

So we get to Kate. Right out of the box, even in its Kate 3 incarnation, I loved Kate. Standalone was good, in KDevelop it was even better. I didn&#8217;t miss the relative lack of extensibility because well, it did pretty much everything I wanted. And then I changed job&#8230;