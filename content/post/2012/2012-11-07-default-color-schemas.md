---
title: Default Color Schemas
author: Dominik Haumann

date: 2012-11-07T11:25:48+00:00
url: /2012/11/07/default-color-schemas/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
With KDE 4.10, the naming of the color schemas in Kate Part changed. Instead of having &#8220;<app name> &#8211; Normal&#8221; and &#8220;<app name> &#8211; Printing&#8221; we now just have &#8220;Normal&#8221; and &#8220;Printing,&#8221; meaning that all applications using Kate Part now share these color schemas. In other words: If you change the Normal schema in KDevelop, you also will see these changes in Kile, Kate, or any other application that embeds Kate Part. If you just want to change the color schema for a single application, create a new color schema and then use this new schema as default!

Besides that, [it&#8217;s been a looong way][1], but finally Kate will ship several default color schemas in KDE 4.10:

<figure id="attachment_2149" aria-describedby="caption-attachment-2149" style="width: 450px" class="wp-caption aligncenter"><img class="size-full wp-image-2149" title="Default Schema Normal" src="/wp-content/uploads/2012/11/normal.png" alt="" width="424" height="300" srcset="/wp-content/uploads/2012/11/normal.png 424w, /wp-content/uploads/2012/11/normal-300x212.png 300w" sizes="(max-width: 424px) 100vw, 424px" /><figcaption id="caption-attachment-2149" class="wp-caption-text">Normal Color Schema</figcaption></figure>

<figure id="attachment_2150" aria-describedby="caption-attachment-2150" style="width: 450px" class="wp-caption aligncenter"><img class="size-full wp-image-2150" title="Solarized (light)" src="/wp-content/uploads/2012/11/solarized.png" alt="" width="424" height="300" srcset="/wp-content/uploads/2012/11/solarized.png 424w, /wp-content/uploads/2012/11/solarized-300x212.png 300w" sizes="(max-width: 424px) 100vw, 424px" /><figcaption id="caption-attachment-2150" class="wp-caption-text"><a title="Solarized Color Schema" href="http://ethanschoonover.com/solarized" target="_blank">Solarized (light)</a></figcaption></figure>

<figure id="attachment_2151" aria-describedby="caption-attachment-2151" style="width: 450px" class="wp-caption aligncenter"><img class="size-full wp-image-2151" title="Vim (dark)" src="/wp-content/uploads/2012/11/vim.png" alt="" width="424" height="300" srcset="/wp-content/uploads/2012/11/vim.png 424w, /wp-content/uploads/2012/11/vim-300x212.png 300w" sizes="(max-width: 424px) 100vw, 424px" /><figcaption id="caption-attachment-2151" class="wp-caption-text">Vim (dark)</figcaption></figure>

The colors are not all perfect, so we hope to improve them over time, so if you always wanted to [contribute to Kate][2], we could need help here :-)

 [1]: /2012/03/07/some-kate-color-schemas/ "Kate Color Schemas"
 [2]: /join-us/ "Contribute to Kate"