---
title: Disable Line Modification Indicators
author: Dominik Haumann

date: 2012-02-26T12:15:17+00:00
url: /2012/02/26/disable-line-modification-indicators/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users

---
On KDE 4.8.0, there is no way to disable the [line modification markers][1].

In KDE >= 4.8.1, you can disable them as follows by first closing Kate and then typing

<pre>kwriteconfig --file katerc --group "Kate View Defaults" --key "Line Modification" --type bool false</pre>

To enable it again, close Kate and run

<pre>kwriteconfig --file katerc --group "Kate View Defaults" --key "Line Modification" --type bool true</pre>

However, this only affects Kate; not KWrite, Kile, KDevelop or any other application using Kate Part. If you want to disable the markers for KWrite, Kile or KDevelop, use kwriterc, kilerc or kdeveloprc instead of katerc.

In KDE >= 4.9, there is a graphical option in the editor configuration dialog in &#8220;Appearance > Borders > [x] Show line modification markers&#8221;.

<pre></pre>

 [1]: /2011/09/06/line-modification-system/ "Line Modification Markers"