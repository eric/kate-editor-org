---
title: Passive Notifications in Kate Part
author: Dominik Haumann

date: 2012-11-06T13:06:11+00:00
url: /2012/11/06/passive-notifications-in-kate-part/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
In KDE 4.10, Kate Part got a new <a title="KTextEditor::MessageInterface in KDE 4.10" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/entry/part/kte5/messageinterface.h" target="_blank">MessageInterface</a>. The idea of this interface is to have a standardized way of showing notifications in a non-intrusive way, similar to how <a title="API of KMessageWidget" href="http://api.kde.org/4.x-api/kdelibs-apidocs/kdeui/html/classKMessageWidget.html" target="_blank">KMessageWidget in kdelibs</a> works. Using the MessageInterface is extremly easy, for instance to show a quick notification, you write

<pre style="padding-left: 30px;">KTextEditor::Message *message = new KTextEditor::Message(KTextEditor::Message::Positive,
                                                         "Code successfully reformated.");
document-&gt;postMessage(message);</pre>

That&#8217;s basically it! The result looks like this (with a nice fade-in and fade-out animation):<img class="aligncenter size-full wp-image-2132" title="Positive Notification Message" src="/wp-content/uploads/2012/11/positive.png" alt="" width="537" height="291" srcset="/wp-content/uploads/2012/11/positive.png 537w, /wp-content/uploads/2012/11/positive-300x162.png 300w" sizes="(max-width: 537px) 100vw, 537px" />

As developer, once you called postMessage(), you can safely forget the message pointer. It&#8217;s automatically deleted either if the user closes the notification, or if the document is closed or reloaded. The API for more tricks. You can add multiple actions through KTextEditor::Message::addAction(), which then appear as buttons. For each added action, you can control whether triggering the action should close the widget or not.

Further, you can set an auto hide timer through Message::setAutoHide(int timeInMilliSeconds).

If you want a message to only appear in one specific KTextEditor::View of the Document, you just call Message::setView() before calling postMessage(). If several messages are in the queue, the one with the highest Message::priority() is shown first.

This notification system is already used in Kate Part by the [data recovery (swap files)][1], or to show a [notification when loading remote files][2], and in some other places.

The cool thin is that this solution requires no special code in KateView or other classes, hence, it&#8217;s a perfectly clean solution with an easy-to-use API. You as KTextEditor user (developer) should definitely consider using it when it&#8217;s public.

**Note**: The MessageInterface is a KTextEditor interface; however, since the 4.10 branch is frozen for BIC changes, we cannot just add this interface for KDE 4.10. Instead, for Kate Part users (Kile, KDevelop, &#8230;) this interface will only be available in KDE5.

Finally, an example of all four message types: Positive, Information, Warning and Error:

<img class="aligncenter size-full wp-image-2135" title="Message Types" src="/wp-content/uploads/2012/11/messages1.png" alt="" width="565" height="500" srcset="/wp-content/uploads/2012/11/messages1.png 565w, /wp-content/uploads/2012/11/messages1-300x265.png 300w" sizes="(max-width: 565px) 100vw, 565px" /> 

<p style="text-align: center;">

 [1]: /2012/10/25/data-recovery-in-4-10/ "Data Recovery in KDE 4.10"
 [2]: /2012/11/05/loading-remote-files/ "Loading Remote Files"