---
title: Project Management
author: Christoph Cullmann

date: 2012-08-05T13:31:06+00:00
url: /2012/08/05/project-management/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
Some years ago the Kate team introduced the &#8220;Session&#8221; feature.

This allows you easily to keep around different Kate sessions for different work tasks, like coding, web design, writing, &#8230;

Whereas I used that feature a lot at start during my normal daily work, I kind of no longer used it already since some years.

For my normal work pattern: Checkout a project, fix something in it or code an extension, then hop over to the next one, the session feature doesn&#8217;t really provide the right workflow.

My checkouts not always stay at the same location on disk, I keep multiple different checkouts of the same project around (or different clones) and I have too many different projects in company to create for all of them a session and keep any overview.

Even more years ago I wrote some project plugin for Kate allowing you to manage a more IDE style kind of project in Kate.

Instead of global sessions in your user account Kate allowed you to create project files which you can check in with your individual projects into your version control.

I purged that plugin later on again, as I lost interest in it because of one major design flaw: You really needed to use the UI to configure your project and add your stuff. That is REALLY annoying if your code base in in a state of flux or evolution and things really keep changing. (The removal wasn&#8217;t that well [perceived][1], btw., sorry, but without maintainer, such plugins really cause a lot of pain.)

Now, here we are, six years later :) Given I still have my itch to scratch, I decided to give the project plugin a new try. But this time with a completely different approach: don&#8217;t create any UI to create/modify projects but come up with a simple file format for them that a Kate plugin can load.

And yeah, given my history as researcher in compiler construction area, what would be better than write just and other parser for some not understandable language? But no, I want to focus on the real stuff, make the description of the projects lightweight and easy to understand and not try to invent some new syntax yet again.

Therefore I settled with just using [JSON][2] to describe the project and started to implement some simple format, like:

<pre>{
    "name": "Kate",

    "projects": [
        {
            "name": "KTextEditor",            
            "files": [ { "directory": "ktexteditor", "filters": ["*.cpp", "*.h"] } ]
        },

        {
            "name": "KatePart",            
            "files": [ { "directory": "part", "filters": ["*.cpp", "*.h"], "recursive": 1 } ]
        },

        {
            "name": "Kate",            
            "files": [ { "directory": "kate", "filters": ["*.cpp", "*.h"], "recursive": 1 } ]
        },

        {
            "name": "KWrite",            
            "files": [ { "directory": "kwrite", "filters": ["*.cpp", "*.h"] } ]
        }
    ]
}</pre>

This small description should be enough to create the project &#8220;Kate&#8221; in my kate.git, containing 4 different toplevel parts: KTextEditor, KatePart, Kate and KWrite.

Without many hassle it just collects everything I need for daily work and constructs some tree structure (hehe, that part is not really done at the moment, but the output in my konsole looks OK).

Actually for my normal company work that format suites even better, as the projects there are normally even more fine grained than kate.git is (and for kate.git subprojects in kate/part/kwrite would even make more sense, above that is just an example).

Unfortunately there is not much done atm, beside some basic format handling in JSON and konsole output of the results, but I hope more will be done in the next weeks in my scratch repo.

 [1]: /2006/07/20/kate-project-plugin/
 [2]: http://www.json.org/