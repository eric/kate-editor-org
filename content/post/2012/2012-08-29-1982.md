---
title: Kate Script IDE Plugin
author: Maximilian

date: 2012-08-29T21:05:49+00:00
url: /2012/08/29/1982/
pw_single_layout:
  - "1"
categories:
  - Common

---
Hello everyone! In this post i am going to introduce the new Scripting IDE Plugin for Kate. Although scripting KatePart via ECMAScript is a powerful feature that has been available for quite some time now, it is still rarely used. In order to make it appealing for a wider public, the Script IDE aims at supporting the user with the process of creating and managing scripts.

The Plugin mainly consists of a tree view which shows currently available scripts, as well as functions contained in these scripts.  
[<img class="size-medium wp-image-2006 aligncenter" src="/wp-content/uploads/2012/08/scriptide1-300x186.png" alt="" width="300" height="186" srcset="/wp-content/uploads/2012/08/scriptide1-300x186.png 300w, /wp-content/uploads/2012/08/scriptide1-1024x635.png 1024w, /wp-content/uploads/2012/08/scriptide1.png 1249w" sizes="(max-width: 300px) 100vw, 300px" />][1]  
Additional scripts can be imported and new scripts can be created via a dialog, which requests necessary information about the script. Supported is the creation of all three types of scripts used in KPart: indentation scripts, command line scripts and API scripts. Further, help for command line scripts will be editable via the GUI.  
[<img class="size-medium wp-image-2007 aligncenter" src="/wp-content/uploads/2012/08/scriptide2-300x283.png" alt="" width="300" height="283" srcset="/wp-content/uploads/2012/08/scriptide2-300x283.png 300w, /wp-content/uploads/2012/08/scriptide2.png 648w" sizes="(max-width: 300px) 100vw, 300px" />][2]  
After i have finished some crucial refactoring, the future plan for Scripting IDE features involves auto-completion for available APIs in the document view, as long as the Script IDE Plugin is loaded. Also, some restructuring of the way KPart handles scripts will most likely be necessary.

As soon as there are new major features implemented, i will post them here. Everyone&#8217;s welcome to comment with his own ideas!

 [1]: /wp-content/uploads/2012/08/scriptide1.png
 [2]: /wp-content/uploads/2012/08/scriptide2.png