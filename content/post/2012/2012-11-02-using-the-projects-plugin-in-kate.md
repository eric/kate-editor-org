---
title: Using the Projects Plugin in Kate
author: Dominik Haumann

date: 2012-11-02T15:42:26+00:00
url: /2012/11/02/using-the-projects-plugin-in-kate/
pw_single_layout:
  - "1"
categories:
  - Events
  - Users
tags:
  - planet

---
<span style="color: #ff0000;"><strong>Update (2014-10-18):</strong></span> [Auto-Loading Projects in Kate 5][1]

### Introducing the Projects Plugin for KDE SC 4.10

The basic idea of the Projects plugin is to have a structured list of files belonging to the  project with the following properties:

  1. provide a structured view of the files
  2. make it easy and very fast to open and switch projects
  3. support search & replace for a project
  4. provide simple auto completion
  5. make it simple to quickly open files in the project
  6. support for building the project

We&#8217;re going to address all these points now.

##### 1. Provide a Structured View of the Files

Once the &#8220;Projects Plugin&#8221; is loaded in the Kate config page, a sidebar appears that lists all projects as well as the projects files as follows:

[<img class="aligncenter size-full wp-image-2094" title="Project Plugin View" src="/wp-content/uploads/2012/11/projects1.png" alt="" width="832" height="533" srcset="/wp-content/uploads/2012/11/projects1.png 832w, /wp-content/uploads/2012/11/projects1-300x192.png 300w" sizes="(max-width: 832px) 100vw, 832px" />][2]

As you can see, the currently active project is &#8220;Kate&#8221;, and it&#8217;s contents is listed in the tree view. Clicking on files in the tree view opens the file in the editor. Further, a context menu is provided with which you can open files with other applications, such as a .ui file with Qt Designer.

##### 2. Switching Projects

The idea is that you never have to open a project manually, this is even not supported at all. Hence, what happens if you open a file, the Project Plugin quickly scans the folder and its parent folders for a .kateproject file. If found, the project is automatically loaded. Very simple and efficient.

Further, if you open another document in Kate, that belongs to another project, the Project plugin automatically switches the current project. So intuitively, always the correct project is active. Of course, you can also switch the currently active project through the combo box.

##### 3. Search & Replace in Projects

Kate has a &#8220;Search & Replace&#8221; plugin that shows up in the bottom sidebar. If a project is loaded and you open the &#8220;Search & Replace&#8221; sidebar, it automatically switches to the mode to search & replace in the current project, which usually is exactly what you want:

[<img class="aligncenter size-full wp-image-2095" title="Search & Replace in Project" src="/wp-content/uploads/2012/11/projects2.png" alt="" width="832" height="533" srcset="/wp-content/uploads/2012/11/projects2.png 832w, /wp-content/uploads/2012/11/projects2-300x192.png 300w" sizes="(max-width: 832px) 100vw, 832px" />][3]

##### 4. Simple Auto Completion

With the knowledge of all files belonging to a project, the Project plugin provides simple auto completion facilities based on ctags. If a project is initially opened, ctags parses all project files in a background thread and saves the ctags information to /tmp. This file then is used to populate the auto completion popup in Kate. In contrast, without this auto completion, Kate was only capable of showing auto completion items based on the words in the current file. So the auto completion provided by the Project plugin is much more powerful.

[<img class="aligncenter size-full wp-image-2096" title="Project Auto Completion" src="/wp-content/uploads/2012/11/projects3.png" alt="" width="832" height="533" srcset="/wp-content/uploads/2012/11/projects3.png 832w, /wp-content/uploads/2012/11/projects3-300x192.png 300w" sizes="(max-width: 832px) 100vw, 832px" />][4]

If ctags is missing, a passive popup warns you about this. It is also noteworthy, that the ctags file in /tmp is cleaned up when Kate exists, so you the plugin does not pollute any folder with unwanted files.

##### 5. Quick Opening Files

As clicking on files in the tree view is not the fastest way to open a file, Kate provides a built-in quick open mechanism you can activate with CTRL+ALT+o. What you the get is a list like this:

[<img class="aligncenter size-full wp-image-2097" title="Quick Open in Project" src="/wp-content/uploads/2012/11/projects4.png" alt="" width="832" height="533" srcset="/wp-content/uploads/2012/11/projects4.png 832w, /wp-content/uploads/2012/11/projects4-300x192.png 300w" sizes="(max-width: 832px) 100vw, 832px" />][5]

You can filter by typing parts of the file name you are looking for, and you can also navigate with the arrow keys and page up/down through the list. Hitting enter activates the selected file, while escape hides the quick open view again.

Further, the quick open remembers the previous file. So when you change to the quick open view the previously activated  file is automatically selected and you just need to hit enter, which comes very handy at times.

##### 6. Support for Building the Project

Another feature is to have support in the Build plugin, so that it automatically is configured correctly.

### Creating Projects

Currently, creating a project requires some manual work, although it is really not much. You just have to create a .kateproject file in the root folder of the project. For instance, the Kate .kateproject file looks like this:

> <pre>{
  "name": "Kate"
, "files": [ { "git": 1 } ]
}</pre>

As you may have noted, this is JSON syntax (because there is a nice jason library for Qt to parse this). The project name is &#8220;Kate&#8221;, and the files contained in should be read from git.

What&#8217;s also supported instead of &#8220;git&#8221; is subversion through &#8220;svn&#8221; and mercurial through &#8220;hg&#8221; (btw, it would be nice to have support for other version control systems as well \*hint\*). If you do not want to read from a version control system, you can tell it to recursively load files from directories as follows:

<pre style="padding-left: 30px;">{
  "name": "Kate"
, "files": [ { "directory": "kate", "filters": ["*.cpp", "*.h", "*.ui", "CMakeLists.txt", "Find*.cmake"], "recursive": 1 } ]
}</pre>

Here, subfolders and filters define what&#8217;s part of the project. You can also [mix version control and files based on filters][6].

If you want to add support for the Build plugin, you could write a .kateconfig like this:

<pre style="padding-left: 30px;">{
  "name": "Kate"
, "files": [ { "git": 1 } ]
, "build": {
    "directory": "build"
  , "build": "make all"
  , "clean": "make clean"
  , "install": "make install"
  }
}</pre>

Hopefully, these examples help you to setup your own project.

##### Kudos

Kudos go to Christoph, he implemented all the features in here. The Project plugin will be shipped with  KDE SC 4.10, and you should really [try it out][7] :-)

 [1]: /2014/10/12/autoloading-projects-plugin-kate-5/ "Auto-loading Projects in Kate 5"
 [2]: /wp-content/uploads/2012/11/projects1.png
 [3]: /wp-content/uploads/2012/11/projects2.png
 [4]: /wp-content/uploads/2012/11/projects3.png
 [5]: /wp-content/uploads/2012/11/projects4.png
 [6]: /2012/08/05/project-management/ "More complex .kateproject files"
 [7]: /get-it/ "Building Kate"