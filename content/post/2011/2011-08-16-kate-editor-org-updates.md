---
title: kate-editor.org Updates
author: Christoph Cullmann

date: 2011-08-16T18:44:22+00:00
url: /2011/08/16/kate-editor-org-updates/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After adding the [new design][1] to the Kate homepage, I got some feedback about broken fonts and other stuff. Most should be fixed now, e.g. the [&#8220;The Team&#8221;][2] page now looks kind of okay again.

In addition, we now have some nice feed display with the latest commits and overall a bit cleanup page. For mobile devices, like my Android phone, we now have [WPtouch][3] installed, which provides some rather nice layout for small screens (but allows easy switch back to normal layout, too).

If you have any additional comments, what could be changed, just write me a comment or a [mail][4].

 [1]: /2011/08/15/good-morning-kate-new-day-new-design/
 [2]: /the-team/
 [3]: http://wordpress.org/extend/plugins/wptouch/
 [4]: mailto:cullmann@kde.org