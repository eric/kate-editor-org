---
title: Kate in Berlin
author: Dominik Haumann

date: 2011-08-15T08:46:56+00:00
url: /2011/08/15/kate-in-berlin/
pw_single_layout:
  - "1"
categories:
  - Events
tags:
  - planet

---
Just as proof that we really were at the desktop summit in Berlin: We Kate developers sat one evening together for dinner. From left to right: Kate&#8217;s plugin master, the [Holy][1] [Belliness][2], KDevelop, Gnome, Kate Finder, Kate Vi Mode, Kate Snippets (and myself not visible, taking the photo) :-)

<p style="text-align: center;">
  <img class="aligncenter size-full wp-image-1257" title="Kate Developers Dinner" src="/wp-content/uploads/2011/08/P1050036.jpg" alt="" width="800" height="600" srcset="/wp-content/uploads/2011/08/P1050036.jpg 800w, /wp-content/uploads/2011/08/P1050036-300x225.jpg 300w" sizes="(max-width: 800px) 100vw, 800px" />
</p>

 [1]: /2010/07/16/the-holy-church-of-kate/
 [2]: /2009/09/18/news-from-the-holy-kate-land/