---
title: GSoC 2011 – Kate Code Folding – week 4 (Integrating the folding algorithm)
author: Adrian Lungu

date: 2011-07-04T11:46:56+00:00
url: /2011/07/04/gsoc-2011-–-kate-code-folding-–-week-4-integrating-the-folding-algorithm/
categories:
  - Developers
  - KDE
  - Users

---
Hi!

I bring good news about Kate’s folding today. The algorithm was implemented and tested successfully. ;)  
As I mentioned in the previous article, I implemented it in a new (simpler) project. It is a Qt project (developed using Qt Creator) and you can find it in [my Kate clone][1], “tree_alg” folder. 

Actually, I have implemented two folding algorithms: one that uses a tree that changes itself dynamically whenever you insert or delete a node, and one based on stack that rebuilds the entire folding tree whenever you insert or delete a node. I needed two algorithms because I wanted to implement an automatic testing mechanism and for that I needed a way to test the output. The second algorithm was easier to be implemented and it was very stable (bug-less) from the beginning, so it was great to use it for my testing.

The automatic testing proved to be a success. I hope I found all the bugs, but I would like to invite you to download the project and to play a little with this application. If you do find any bugs, please send the history files on the mailing list and I will fix them. About these history files, when you save the history two files will be created. E.g.: if you save your history in a file called “history\_1”, then you will have (in the same directory) a file called “history\_1” and a file called “history\_1\_moves”. Please send them both if you notice any bug. It will make my debugging much easier. 

My next move is to integrate this tree folding algorithm into Kate’s folding. For this part, I will have to finish an undone job from a previous week (rewrite the interface and the update methods) and add the algorithm I have just implemented.  
I think that in ten days at most we will have a new code folding for c style languages. :)

All the best,  
Adrian

 [1]: http://quickgit.kde.org/?p=clones%2Fkate%2Flungu%2Fcode_folding.git&a=summary