---
title: GSoC 2011 – Kate Code Folding – week 3 (Folding algorithm started)
author: Adrian Lungu

date: 2011-06-24T20:08:34+00:00
url: /2011/06/24/gsoc-2011-–-kate-code-folding-–-week-3-folding-algorithm-started/
categories:
  - Developers
  - KDE
  - Users

---
Hello guys!

I’m writing my weekly article today because I have already started working on something else. I didn’t have time to finish the previous stage because I started the next phase of the project.

I had a talk with my GSoC mentor and a couple of Kate developers and we all concluded that I should start working on the folding algorithm as soon as possible because this is the main (and most important) part. For this part, I built a small new project that will help me implement the algorithm and test it independently from Kate project. You can find this project (and my Kate clone) at this [address][1]. There are not so many methods implemented, but you can figure out how things will be developed.

Fortunately, I don’t have to build this algorithm from scratch. I made some research and had some results by the time I was working on my proposal. [Here][2] is the paper I wrote based on that research and [here][3] is my GSoC proposal, too (it is public now, so anyone can see it).

If you have any questions or ideas, feel free to leave a comment here or to send an e-mail on Kate’s mailing list.

I’ll keep you in touch with my progress,

Adrian

 [1]: http://quickgit.kde.org/?p=clones%2Fkate%2Flungu%2Fcode_folding.git&a=summary
 [2]: /wp-content/uploads/2011/06/folding-tree-algorithm-paper.pdf
 [3]: http://www.google-melange.com/gsoc/proposal/review/google/gsoc2011/adrian_lungu89/1