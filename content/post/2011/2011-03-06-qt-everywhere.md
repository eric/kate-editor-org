---
title: Qt Everywhere?
author: Christoph Cullmann

date: 2011-03-06T15:40:25+00:00
url: /2011/03/06/qt-everywhere/
categories:
  - Developers
  - KDE
tags:
  - planet

---
After the latest changes in Nokia, I was kind of scared that &#8220;Qt Everywhere!&#8221; like printed on my nice bathing towel won&#8217;t really happen any more, at least not as fast as thought.

Now I got my new phone, Android based, and tried out [Necessitas][1]. And I must say, I am impressed. Just downloaded Ministro from the Android Market (yes, just like that, no rooting, no hacking, nothing) and the hello world demo and it runs :P

I hope this project will really take off even more as soon as a stable Qt 4.8 is around! And that we see some KDE fame there, too. Really, this is not to be underestimated. The Android market share grows and the current market policies allow open source there, unlike what we see for the WP7 or iOS systems.

Already now: Thanks to the Necessitas team, great work! Really necessary ;)

 [1]: http://sourceforge.net/p/necessitas/home/