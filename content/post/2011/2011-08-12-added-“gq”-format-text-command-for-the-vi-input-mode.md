---
title: Added “gq” (format text) command for the Vi Input Mode
author: Erlend Hamberg

date: 2011-08-12T13:14:31+00:00
url: /2011/08/12/added-“gq”-format-text-command-for-the-vi-input-mode/
categories:
  - Common
  - KDE
  - Users
tags:
  - planet
  - vi input mode

---
It&#8217;s been a nice week here in Berlin at the Desktop summit. I have even got some coding done. A command I often missed from Vim was the **gq** command that re-formats text based on your text width setting. For example, if you want to stick to a sixty-columns layout, you often have to re-format the text when adding or removing something, like in this screenshot where some text has been surrounded by HTML tags:

[<img class="aligncenter size-medium wp-image-1201" title="before_wrap" src="/wp-content/uploads/2011/08/before_wrap-300x142.png" alt="" width="300" height="142" srcset="/wp-content/uploads/2011/08/before_wrap-300x142.png 300w, /wp-content/uploads/2011/08/before_wrap-1024x485.png 1024w, /wp-content/uploads/2011/08/before_wrap.png 1151w" sizes="(max-width: 300px) 100vw, 300px" />][1]

By using the **gq** command, the text will be re-formatted and looks nice again:

[<img class="aligncenter size-medium wp-image-1202" title="after_wrap" src="/wp-content/uploads/2011/08/after_wrap-300x180.png" alt="" width="300" height="180" srcset="/wp-content/uploads/2011/08/after_wrap-300x180.png 300w, /wp-content/uploads/2011/08/after_wrap.png 1019w" sizes="(max-width: 300px) 100vw, 300px" />][2]

I have [implemented this command][3] now, so this will be in Kate for KDE 4.8. I hope other users of the Vi Input Mode find it useful too. :-)

 [1]: /wp-content/uploads/2011/08/before_wrap.png
 [2]: /wp-content/uploads/2011/08/after_wrap.png
 [3]: http://commits.kde.org/kate/7cd0b5d3747c7bdaf2e5f2a08ec562dc9d97c6ad