---
title: GSoC’s ending
author: Christoph Cullmann

date: 2011-08-18T11:42:35+00:00
url: /2011/08/18/gsocs-ending/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Soon the GSoC projects of this year are over again.

I would like to thank our two students Adrian Lungu and Svyatoslav Kuzmich for their work!

They both did a nice job and improved both the [code folding][1] and the [vi mode][2] a lot.

Thanks to them and I hope they stick around in our project, more help is always welcome ;)

And thanks to Google for sponsoring their efforts.

 [1]: /2011/08/15/the-gsoc-season-is-ending%E2%80%A6/
 [2]: /2011/08/07/vitest/