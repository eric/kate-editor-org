---
title: Kate Vi Mode Test Suite – GSoC 2011
author: Svyatoslav Kuzmich

date: 2011-08-07T09:06:02+00:00
url: /2011/08/07/vitest/
categories:
  - Developers

---
This summer vi mode has a new test suite. Now there are over 250 tests and the number of them still growing.  
It&#8217;s very easy to add a new test. All you need is just to add the  
DoTest(&#8220;Original text&#8221;,&#8221;Vi command&#8221;,&#8221;Expected text after doing vi command&#8221;);  
line to [part][1]/[tests][2]/[vimode_test.cpp][3].  
Format for command with CTRL &#8211; modifier: **\\ctrl-x**, for command -mode command: **\\:command\\**.  
There are little restriction: you can&#8217;t input text while being in input mode.

Exampls:  
DoTest(&#8220;foobar&#8221;,&#8221;Vypp&#8221;,&#8221;foobar\nfoobar\nfoobar&#8221;);  
DoTest(&#8220;foo\nbar&#8221;,&#8221;\\ctrl-vjl~&#8221;,&#8221;FOo\nBAr&#8221;);  
DoTest(&#8220;foo\nbar\nbaz&#8221;,&#8221;\\:2,3delete\\&#8221;,&#8221;foo&#8221;);

It&#8217;s also a good format for bug report or for feature wish for vi-mode.

 [1]: https://projects.kde.org/projects/kde/kdebase/kate/repository/revisions/master/show/part
 [2]: https://projects.kde.org/projects/kde/kdebase/kate/repository/revisions/master/show/part/tests
 [3]: https://projects.kde.org/projects/kde/kdebase/kate/repository/revisions/master/changes/part/tests/vimode_test.cpp