---
title: Kate Modeline Editor
author: Dominik Haumann

date: 2011-07-23T15:34:40+00:00
url: /2011/07/23/kate-modeline-editor/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
<a title="KDE Commit Digest" href="http://commit-digest.org/issues/2011-07-17/" target="_blank">As you may know</a>, Kate supports the concept of document variables, also known as [modelines][1]. In short, document variables can be used to set Kate settings local to a file. For instance, this is useful for settings specific indenter options than the ones defined in Kate&#8217;s settings dialog. Assume you work on two projects, the first indents with spaces and the second with tabs, then you can simply add a modeline that contains the specific document variables that set the specific indentation properties.

While the concept is very mighty, it is not intuitive to use: You have to know the exact syntax and correct varialbes and their valid values.

To improve this for the next KDE release (v4.8?), Kate <a title="Git Commit" href="https://projects.kde.org/projects/kde/kdebase/kate/repository/revisions/b3eb57233bd3504415167669e02c75c92841a98b" target="_blank">gained</a> a variable / modeline editor this week:  
[<img src="/wp-content/uploads/2011/07/kate-modeline-editor.png" alt="" title="kate modeline editor" width="818" height="617" class="alignnone size-full wp-image-1578" srcset="/wp-content/uploads/2011/07/kate-modeline-editor.png 818w, /wp-content/uploads/2011/07/kate-modeline-editor-300x226.png 300w" sizes="(max-width: 818px) 100vw, 818px" />][2]

If you click the Edit button, a list view pops up showing all valid document variables with the valid values. Right now, this editor is included only in the &#8220;Modes & Filetypes&#8221; config page. However, the idea is to support it in the context menu over a Kate modeline in the text editor area as well.

 [1]: /2006/02/09/kate-modelines/ "Kate Document Variables"
 [2]: /wp-content/uploads/2011/07/kate-modeline-editor.png