---
title: Akademy 2018 Wrap-Up
author: Christoph Cullmann

date: 2018-08-17T15:06:46+00:00
url: /2018/08/17/akademy-2018-wrap-up/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
The [Akademy 2018][1] ends today.

Like each Akademy I attended, it was an interesting experience. As the location switches around each year, so does the set of people attending change every year, too.

That is actually nice, as you get always to meet some of your old &#8220;friends&#8221; but additionally new members of the KDE community. I think this kind of &#8220;conferences&#8221; or &#8220;meetings&#8221; are an important way to get some more cohesion in the community, which is sometimes a bit lacking between people only meeting online via mail/&#8230;

Beside the presentation tracks and the e.V. meeting, several of the [BoFs][2] did spark my interest.

In the KDevelop BoF, Sven talked about what could be done to give the current KDevelop project a bit more focus on the parts it does well to polish them more for a better user experience. The idea is that if you get KDevelop shining even more in the areas it is good at and perhaps cut off some parts that are really given bad impressions, one might attract more people to both use it and contribute. It is still to be discussed if this idea is shared with the other KDevelop contributors.

In the kdesrc-build BoF Michael talked about the current state and collected pain points from the audience and potential future extensions. For example an API to allow to build a light-weight GUI tooling around kdesrc-build to ease the entry to the KDE development was one topic of interest.

Between the conference/BoF/socializing parts of Akademy, I got plenty of time to finally work again on some KTextEditor/Kate related tasks.

With help of Dominik and Volker I got to [integrate the KSyntaxHighlighting framework][3] and we even got at least some initial contact with the QtCreator team about the topic of integrating this framework to replace their own implementation of the Kate syntax definition handling. If you experience any issues with the highlighting or folding in the master branch, please file a bug or even better provide some patch on [phabricator][4].

In addition some small KTextEditor and Kate bugs got either solved or at least started to be worked on again. Help with any bug fixing is always welcome!

As small but perhaps for users important step was to actually [link to the new and shiny Windows installers][5] that the [Binary Factory for KDE][6] produces. Thanks to the team behind that, once more. Hopefully that will lead to more users and developers for Kate on Windows.

Thanks to the organizers to make this Akademy happen and all people that volunteered! Great job! The [sponsors][7] are highly appreciated for their contributions, too.

So, thanks for all the fish (or Krapfen), lets see how Akademy next year will be :=)

<img class="aligncenter size-medium wp-image-4142" src="/wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg" alt="" width="300" height="74" srcset="/wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg 300w, /wp-content/uploads/2018/07/going_to_akademy_banner-768x188.jpg 768w, /wp-content/uploads/2018/07/going_to_akademy_banner.jpg 840w" sizes="(max-width: 300px) 100vw, 300px" />

 [1]: https://akademy.kde.org/2018
 [2]: https://www.youtube.com/watch?v=_yyOc6-x2yI&list=PLsHpGlwPdtMraXbFHhkFx7-QHpEl9dOsL
 [3]: /2018/08/14/porting-ktexteditor-to-ksyntaxhighlighting-done/
 [4]: https://phabricator.kde.org
 [5]: /get-it/
 [6]: /2018/08/16/akademy-binary-factory/
 [7]: https://akademy.kde.org/2018/sponsors