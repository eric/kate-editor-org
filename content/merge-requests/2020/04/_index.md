---
title: Merge Requests - April 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/04/
---

#### Week 17

- **Kate - [Initialize overlooked variables (and remove redundant calls)](https://invent.kde.org/utilities/kate/-/merge_requests/74)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after 20 days.

- **Kate - [Highlighting Doc: add XML files directory of Kate&#x27;s Flatpak/Snap package](https://invent.kde.org/utilities/kate/-/merge_requests/75)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

