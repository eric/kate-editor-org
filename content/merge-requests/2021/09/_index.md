---
title: Merge Requests - September 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/09/
---

#### Week 39

- **Kate - [Fix leak in color picker plugin](https://invent.kde.org/utilities/kate/-/merge_requests/492)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 38

- **Kate - [lspclient: avoid use of stale pointer when building code action menu](https://invent.kde.org/utilities/kate/-/merge_requests/489)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [Add basic QML API docs](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/256)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **KTextEditor - [The user is not selecting as soon as we clearSelection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/193)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged at creation day.

#### Week 37

- **Kate - [LSP diagnostics](https://invent.kde.org/utilities/kate/-/merge_requests/485)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 7 days.

#### Week 36

- **Kate - [add a LaTeX unicode completion plugin](https://invent.kde.org/utilities/kate/-/merge_requests/482)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged after 7 days.

- **Kate - [Improve git show file history](https://invent.kde.org/utilities/kate/-/merge_requests/486)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Miscellaneous improvements to search toolview](https://invent.kde.org/utilities/kate/-/merge_requests/473)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 36 days.

- **KTextEditor - [Do not cancel mouse selection when using the keyboard](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/191)**<br />
Request authored by [Aleix Pol Gonzalez](https://invent.kde.org/apol) and merged after 5 days.

- **Kate - [Git: Allow deleting branches](https://invent.kde.org/utilities/kate/-/merge_requests/481)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [yara - add new 4.x keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/254)**<br />
Request authored by [Wes H](https://invent.kde.org/hurdw) and merged after one day.

#### Week 35

- **KSyntaxHighlighting - [Initial work on Terraform syntax highlight](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/255)**<br />
Request authored by [Denis Doria](https://invent.kde.org/thuck) and merged at creation day.

- **KSyntaxHighlighting - [Port AbstractHighlighter::highlightLine to QStringView](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/253)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 2 days.

- **Kate - [Delete unfilled entries in tool config group](https://invent.kde.org/utilities/kate/-/merge_requests/480)**<br />
Request authored by [Christopher Yeleighton](https://invent.kde.org/cyeleighton) and merged after one day.

- **KSyntaxHighlighting - [markdown.xml: Close nested sub-headers fold regions when parent header is closed](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/251)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

