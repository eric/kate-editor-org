---
title: Merge Requests - October 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/10/
---

#### Week 42

- **Kate - [Rename the show untitled document option](https://invent.kde.org/utilities/kate/-/merge_requests/971)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [if a document is only visible in a window, close it with that window](https://invent.kde.org/utilities/kate/-/merge_requests/970)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Better highlighting for some GIS-related file formats](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/373)**<br />
Request authored by [Lukas Sommer](https://invent.kde.org/sommer) and merged after 6 days.

- **Kate - [Fix doc closing when active doc is in multiple viewspaces](https://invent.kde.org/utilities/kate/-/merge_requests/968)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Unify KateTabBar internal tab data](https://invent.kde.org/utilities/kate/-/merge_requests/964)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[WelcomeView] Properly handle changes to the recent items](https://invent.kde.org/utilities/kate/-/merge_requests/965)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [Introduce ByteArraySplitter](https://invent.kde.org/utilities/kate/-/merge_requests/966)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove empty viewspaces after restoring config](https://invent.kde.org/utilities/kate/-/merge_requests/963)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Assign default shortcuts for some actions](https://invent.kde.org/utilities/kate/-/merge_requests/902)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 26 days.

- **Kate - [Fix movable tab resetting](https://invent.kde.org/utilities/kate/-/merge_requests/960)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [KateFileTree: fix renaming files if dest already exists](https://invent.kde.org/utilities/kate/-/merge_requests/962)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Allow detaching search tab to mainWindow](https://invent.kde.org/utilities/kate/-/merge_requests/945)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [[WelcomeView] Polishing](https://invent.kde.org/utilities/kate/-/merge_requests/959)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [Dont call showView() from createView](https://invent.kde.org/utilities/kate/-/merge_requests/961)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Config: Implement searching](https://invent.kde.org/utilities/kate/-/merge_requests/953)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [set activeView to null when a non-KTE::View is active](https://invent.kde.org/utilities/kate/-/merge_requests/958)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[WelcomeView] Add help panel](https://invent.kde.org/utilities/kate/-/merge_requests/957)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **KSyntaxHighlighting - [VHDL: support of VHDL-2008](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/374)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Nix: fix decimal numbers format and some performance changes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/375)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [[WelcomeView] Reduce margins](https://invent.kde.org/utilities/kate/-/merge_requests/956)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KSyntaxHighlighting - [Alerts: add NO-BREAK SPACE (nbsp) as keyword deliminator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/372)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Git Ignore: more highlight / Git Rebase: add missing commands](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/370)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [cmake.xml: New features for CMake 3.25](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/371)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

#### Week 41

- **Kate - [Better code to open folder](https://invent.kde.org/utilities/kate/-/merge_requests/950)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **Kate - [[WelcomeView] Fix placeholder text](https://invent.kde.org/utilities/kate/-/merge_requests/948)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after 2 days.

- **Kate - [[WelcomeView] Fix the text of the recent items label](https://invent.kde.org/utilities/kate/-/merge_requests/951)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **KSyntaxHighlighting - [Bash/Zsh: fix group command in coproc and closing of Parameter Expansion](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/369)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [fix AnsiHighlighter::highlightData() with valid definition, but without any context](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/368)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [use color scheme colors for the notifications](https://invent.kde.org/utilities/kate/-/merge_requests/952)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Port away from deprecated KMessageBox Yes/No](https://invent.kde.org/utilities/kate/-/merge_requests/911)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 15 days.

- **Kate - [Make KateConfigDialog a normal view in kate](https://invent.kde.org/utilities/kate/-/merge_requests/946)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [[WelcomeView] Slightly improve UI](https://invent.kde.org/utilities/kate/-/merge_requests/947)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after one day.

- **Kate - [remove session applet](https://invent.kde.org/utilities/kate/-/merge_requests/849)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 54 days.

- **Kate - [Fix toolbar name of hamburger menu toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/949)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Port away from deprecated KMessageBox Yes/No](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/423)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 15 days.

- **Kate - [support hamburger menu in Kate &amp; KWrite](https://invent.kde.org/utilities/kate/-/merge_requests/899)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 20 days.

- **Kate - [Add a new msg indicator for output toolview tab button](https://invent.kde.org/utilities/kate/-/merge_requests/944)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [add recent projects](https://invent.kde.org/utilities/kate/-/merge_requests/940)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 3 days.

- **Kate - [Build-plugin: Rework build target selection/execution](https://invent.kde.org/utilities/kate/-/merge_requests/939)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 4 days.

- **Kate - [New welcome screen UI](https://invent.kde.org/utilities/kate/-/merge_requests/932)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged after 7 days.

- **Kate - [lspclient: handle server request with string id](https://invent.kde.org/utilities/kate/-/merge_requests/943)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Diff updates](https://invent.kde.org/utilities/kate/-/merge_requests/942)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Completion updates](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/428)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **kate-editor.org - [Update Windows binary URL to a more up-to-dated version](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/44)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged at creation day.

- **Kate - [LSP: Avoid triggering signature help all the time](https://invent.kde.org/utilities/kate/-/merge_requests/938)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Delay project filtering](https://invent.kde.org/utilities/kate/-/merge_requests/941)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Add folding for maps, structs and lists in elixir](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/366)**<br />
Request authored by [Vitor Trindade](https://invent.kde.org/vitortrin) and merged after 6 days.

- **kate-editor.org - [Remove .en part from file names](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/43)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after 4 days.

#### Week 40

- **Kate - [close the first untitled unmodified document on open](https://invent.kde.org/utilities/kate/-/merge_requests/937)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Port away from KDirOperator::actionCollection](https://invent.kde.org/utilities/kate/-/merge_requests/931)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 3 days.

- **Kate - [Enable semantic highlighting by default](https://invent.kde.org/utilities/kate/-/merge_requests/934)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [filetree: small improvements to mini toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/936)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Add opendocument &quot;flat XML&quot; file types](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/367)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [build-plugin: Update run button after model edit](https://invent.kde.org/utilities/kate/-/merge_requests/935)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Add docs for quickopen](https://invent.kde.org/utilities/kate/-/merge_requests/930)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Unify (remove) plugin content margins](https://invent.kde.org/utilities/kate/-/merge_requests/929)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [Remove &quot;Preview:&quot; label in themes tab](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/427)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Handle generation ourselves, let Scripty do extraction only](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/42)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [search: do not run if there are no documents open](https://invent.kde.org/utilities/kate/-/merge_requests/928)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Add more build-plugin target model index checking](https://invent.kde.org/utilities/kate/-/merge_requests/927)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [LSPPlugin: don&#x27;t show a close button on Diagnostics tab](https://invent.kde.org/utilities/kate/-/merge_requests/926)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [project: Add ability to exclude folders](https://invent.kde.org/utilities/kate/-/merge_requests/925)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Emit projectMapChanged when .kateproject is modified](https://invent.kde.org/utilities/kate/-/merge_requests/923)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add Qt6 windows CI support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/426)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Add saving the manually added run commands](https://invent.kde.org/utilities/kate/-/merge_requests/922)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Update our .kateproject, make it actually usable](https://invent.kde.org/utilities/kate/-/merge_requests/924)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build: Allow run commands](https://invent.kde.org/utilities/kate/-/merge_requests/921)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from soon-to-be-deprecated codecForName](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/425)**<br />
Request authored by [Sune Vuorela](https://invent.kde.org/sune) and merged at creation day.

- **Kate - [[WelcomeView] Handle loading/unloading of the Project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/918)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [build: Use QPersistentModelIndex](https://invent.kde.org/utilities/kate/-/merge_requests/920)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[WelcomeView] Improve locating a file in the file manager](https://invent.kde.org/utilities/kate/-/merge_requests/919)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KSyntaxHighlighting - [Add folding to elixir code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/365)**<br />
Request authored by [Vitor Trindade](https://invent.kde.org/vitortrin) and merged at creation day.

#### Week 39

- **Kate - [Improve fuzzy matching](https://invent.kde.org/utilities/kate/-/merge_requests/916)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [quickopen: Allow wildcard matching (optionally)](https://invent.kde.org/utilities/kate/-/merge_requests/912)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix lru tab switching when there are widgets](https://invent.kde.org/utilities/kate/-/merge_requests/913)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Move away from interal fuzzy algo to KFuzzyMatcher](https://invent.kde.org/utilities/kate/-/merge_requests/915)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [ecma_parser: add space after &#x27;class&#x27; and &#x27;function&#x27; keywords](https://invent.kde.org/utilities/kate/-/merge_requests/914)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **KTextEditor - [Restore always current highlight/mode settings on reload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/424)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar) and merged after one day.

- **KTextEditor - [Some tests and small refactor to KateViewInternal::word{Prev,Next}](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/422)**<br />
Request authored by [Daniel Contreras](https://invent.kde.org/inextremares) and merged after 2 days.

- **KSyntaxHighlighting - [Add Qt 6 Windows CI](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/364)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

