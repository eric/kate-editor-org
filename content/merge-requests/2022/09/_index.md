---
title: Merge Requests - September 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/09/
---

#### Week 39

- **Kate - [Assign more shortcuts using Ctrl+T as pseudo meta key](https://invent.kde.org/utilities/kate/-/merge_requests/900)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 5 days.

- **Kate - [Improve fuzzy matching](https://invent.kde.org/utilities/kate/-/merge_requests/910)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [quickopen: extra points for already open files](https://invent.kde.org/utilities/kate/-/merge_requests/905)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Allow code completion using the tab key](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/419)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 4 days.

- **KSyntaxHighlighting - [Bash,Zsh: fix style for )) in $((...)) and add Dollar Prefix style for simple variable](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/362)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [prefer Oklab to CIELAB for perceptual color difference in ansi256 mode](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/361)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [PostgreSQL: Remove # as line comment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/363)**<br />
Request authored by [Daniel Contreras](https://invent.kde.org/inextremares) and merged at creation day.

- **Kate - [filetree: fix closing when close button is disabled](https://invent.kde.org/utilities/kate/-/merge_requests/896)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 3 days.

- **Kate - [Remove unused includes](https://invent.kde.org/utilities/kate/-/merge_requests/906)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

#### Week 38

- **Kate - [Ensure path is shortened correctly](https://invent.kde.org/utilities/kate/-/merge_requests/903)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Sidebar: Set session restore flag at correct place](https://invent.kde.org/utilities/kate/-/merge_requests/904)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar) and merged at creation day.

- **KTextEditor - [Unassign transpose character shortcut](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/420)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **KTextEditor - [clipboardialog: add placerholder label when empty](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/421)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Remove close button from default toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/901)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged at creation day.

- **Kate - [Fixed a null pointer dereference in gdb plugin](https://invent.kde.org/utilities/kate/-/merge_requests/898)**<br />
Request authored by [Ognian Milanov](https://invent.kde.org/ognian) and merged at creation day.

- **Kate - [Sidebar: Don&#x27;t save a session while session restore is running](https://invent.kde.org/utilities/kate/-/merge_requests/895)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar) and merged after one day.

- **Kate - [Add an action to detach a tab](https://invent.kde.org/utilities/kate/-/merge_requests/892)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [lspclient: shorten paths in treeview if possible](https://invent.kde.org/utilities/kate/-/merge_requests/893)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [Fix the tabswitcher prefix in an edge case](https://invent.kde.org/utilities/kate/-/merge_requests/894)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after one day.

- **Kate - [allow to disable the welcome page](https://invent.kde.org/utilities/kate/-/merge_requests/891)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **KTextEditor - [Use consistently std::as_const instead of qAsConst](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/418)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Introduce a welcome page to Kate &amp; KWrite and allow to have no documents/views open](https://invent.kde.org/utilities/kate/-/merge_requests/888)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 2 days.

- **Kate - [LSP client: Improve support for markdown in documentation tooltips](https://invent.kde.org/utilities/kate/-/merge_requests/890)**<br />
Request authored by [Fire Fragment](https://invent.kde.org/firefragment) and merged after one day.

- **KTextEditor - [Add tests and fixes for various indenters](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/416)**<br />
Request authored by [Bharadwaj Raju](https://invent.kde.org/bharadwaj-raju) and merged after 11 days.

#### Week 37

- **Kate - [Ensure consistent behavior for compare actions](https://invent.kde.org/utilities/kate/-/merge_requests/889)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [don&#x27;t enforce all documents have tabs/views in all windows](https://invent.kde.org/utilities/kate/-/merge_requests/879)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 8 days.

- **KSyntaxHighlighting - [.gitlab-ci.yml: enable static builds](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/360)**<br />
Request authored by [Dawid Wróbel](https://invent.kde.org/wrobelda) and merged at creation day.

- **KSyntaxHighlighting - [Improve ASN.1 highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/359)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Fix compilation with KF 5.90](https://invent.kde.org/utilities/kate/-/merge_requests/886)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [php_parser: add trait support](https://invent.kde.org/utilities/kate/-/merge_requests/884)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **Kate - [Rename variable it to idx](https://invent.kde.org/utilities/kate/-/merge_requests/885)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **KSyntaxHighlighting - [Ruby: add keywords used for refinements to the mixin methods list](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/358)**<br />
Request authored by [Georg Gadinger](https://invent.kde.org/ggadinger) and merged after one day.

- **KTextEditor - [Normalize signatures in mainwindow.cpp](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/417)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Python: add := operator, built-in functions and special methods](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/357)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Start git blame with a delay on activeView](https://invent.kde.org/utilities/kate/-/merge_requests/880)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Documents: Dont allow context menu at random places](https://invent.kde.org/utilities/kate/-/merge_requests/881)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Crystal Syntax Highlighting Definition: // operator, macros, keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/356)**<br />
Request authored by [Gaurav Shah](https://invent.kde.org/gshah) and merged after one day.

#### Week 36

- **Kate - [Fix viewspace not closed when last was not a KTextEditor::View](https://invent.kde.org/utilities/kate/-/merge_requests/882)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Show non-KTE::* widgets in documents plugin](https://invent.kde.org/utilities/kate/-/merge_requests/877)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Add interfaces for mainWindow functions instead of using metaobject](https://invent.kde.org/utilities/kate/-/merge_requests/876)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Project: Hide actions that are not applicable](https://invent.kde.org/utilities/kate/-/merge_requests/875)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix annoying build-output scrolling](https://invent.kde.org/utilities/kate/-/merge_requests/872)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [FileHistory improvements](https://invent.kde.org/utilities/kate/-/merge_requests/874)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [debianchangelog.xml: Set default filename](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/354)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [bash.xml: The other (more correct) form of multiline comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/351)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 7 days.

#### Week 35

- **KTextEditor - [add showMessage, to show formatted messages](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/415)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Diff improvements](https://invent.kde.org/utilities/kate/-/merge_requests/870)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add a diff viewer to kate](https://invent.kde.org/utilities/kate/-/merge_requests/865)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [add addWidgetAsTab to MainWindow](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/414)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: Add `CMAKE_FILES_DIRECTORY` as undocumented](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/352)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **KSyntaxHighlighting - [Use C-style indenting for GLSL](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/353)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **Kate - [php_parser: reduce variable scope](https://invent.kde.org/utilities/kate/-/merge_requests/868)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged at creation day.

- **Kate - [bash_parser: reduce scope variable](https://invent.kde.org/utilities/kate/-/merge_requests/869)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged at creation day.

