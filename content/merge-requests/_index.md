---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Currently Open Merge Requests
Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!
### Kate

- **[Add Qt6 windows CI support](https://invent.kde.org/utilities/kate/-/merge_requests/933)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent).

- **[lspclient: add rootfile pattern to detect rootpath](https://invent.kde.org/utilities/kate/-/merge_requests/907)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric).

- **[gdbplugin: fixed interrupt action for DAP backend](https://invent.kde.org/utilities/kate/-/merge_requests/972)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm).

- **[Use KPageDialog::List on KWrite&#x27;s config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/967)**<br />
Request authored by [jrv ezg](https://invent.kde.org/jrv).

- **[Introduce &quot;Changelog/What&#x27;s new&quot; widget](https://invent.kde.org/utilities/kate/-/merge_requests/955)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).

- **[Use KService and ApplicationLauncherJob for diff tool handling](https://invent.kde.org/utilities/kate/-/merge_requests/801)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).

- **[Draft: CMakeTools Plugin Improvement](https://invent.kde.org/utilities/kate/-/merge_requests/792)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric).

- **[Add actions to toggle visibilty of toolviews by sidebar](https://invent.kde.org/utilities/kate/-/merge_requests/834)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).

- **[Add closed files to recent files](https://invent.kde.org/utilities/kate/-/merge_requests/750)**<br />
Request authored by [Raphaël Jakse](https://invent.kde.org/jakse).

### KTextEditor

- **[Remove double message about lack of access rights](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/431)**<br />
Request authored by [Ilya Pominov](https://invent.kde.org/ipominov).

- **[Allow executing process in scripts](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/429)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).

- **[bugs 454312 and 454417: fix gv when selecting backwards, fix vi visual mode mouse selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/430)**<br />
Request authored by [Sarah Wong](https://invent.kde.org/sarwong).

- **[Enable &#x27;indent text on paste&#x27; by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/210)**<br />
Request authored by [Bharadwaj Raju](https://invent.kde.org/bharadwaj-raju).

### KSyntaxHighlighting

- **[CMake: add Control Flow style for if, else, while, etc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/376)**<br />
Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).

- **[powershell.xml: Improvements in strings recongnition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/377)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov).

- **[extend the list of default text styles](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/320)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff).


## Overall Accepted Merge Requests
- 848 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 396 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 355 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 40 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2022

- 368 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 170 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 79 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 8 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [October 2022](/merge-requests/2022/10/) (80 requests)
* [September 2022](/merge-requests/2022/09/) (53 requests)
* [August 2022](/merge-requests/2022/08/) (93 requests)
* [July 2022](/merge-requests/2022/07/) (43 requests)
* [June 2022](/merge-requests/2022/06/) (39 requests)
* [May 2022](/merge-requests/2022/05/) (37 requests)
* [April 2022](/merge-requests/2022/04/) (34 requests)
* [March 2022](/merge-requests/2022/03/) (91 requests)
* [February 2022](/merge-requests/2022/02/) (89 requests)
* [January 2022](/merge-requests/2022/01/) (66 requests)


## Accepted Merge Requests of 2021

- 348 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 177 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 144 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 24 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2021](/merge-requests/2021/12/) (51 requests)
* [November 2021](/merge-requests/2021/11/) (32 requests)
* [October 2021](/merge-requests/2021/10/) (45 requests)
* [September 2021](/merge-requests/2021/09/) (15 requests)
* [August 2021](/merge-requests/2021/08/) (34 requests)
* [July 2021](/merge-requests/2021/07/) (45 requests)
* [June 2021](/merge-requests/2021/06/) (57 requests)
* [May 2021](/merge-requests/2021/05/) (57 requests)
* [April 2021](/merge-requests/2021/04/) (37 requests)
* [March 2021](/merge-requests/2021/03/) (91 requests)
* [February 2021](/merge-requests/2021/02/) (109 requests)
* [January 2021](/merge-requests/2021/01/) (120 requests)


## Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2020](/merge-requests/2020/12/) (58 requests)
* [November 2020](/merge-requests/2020/11/) (28 requests)
* [October 2020](/merge-requests/2020/10/) (36 requests)
* [September 2020](/merge-requests/2020/09/) (42 requests)
* [August 2020](/merge-requests/2020/08/) (56 requests)
* [July 2020](/merge-requests/2020/07/) (10 requests)
* [June 2020](/merge-requests/2020/06/) (8 requests)
* [May 2020](/merge-requests/2020/05/) (14 requests)
* [April 2020](/merge-requests/2020/04/) (2 requests)
* [March 2020](/merge-requests/2020/03/) (4 requests)
* [February 2020](/merge-requests/2020/02/) (7 requests)
* [January 2020](/merge-requests/2020/01/) (5 requests)


## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2019](/merge-requests/2019/12/) (7 requests)
* [November 2019](/merge-requests/2019/11/) (3 requests)
* [October 2019](/merge-requests/2019/10/) (9 requests)
* [September 2019](/merge-requests/2019/09/) (20 requests)
* [August 2019](/merge-requests/2019/08/) (12 requests)
