---
title: Join Us
hideMeta: true
author: Christoph Cullmann
date: 2010-07-09T08:58:54+00:00
menu:
  main:
    weight: 50
    parent: menu
---

### How to help with Kate development

If you want to help us develop Kate, KWrite or KatePart, you should <a title="Kate/KWrite mailing list" href="https://mail.kde.org/mailman/listinfo/kwrite-devel" target="_blank">join our mailing list</a>. Contacting us at <a title="Kate IRC Channel" href="irc://irc.libera.chat/kate" target="_blank">#kate on irc.libera.chat</a> is also a good idea, we are always happy to help you get started.

### How to Build Kate

Kate can easily be build on a current Linux distribution, we provide a [tutorial about building Kate and the needed dependencies](/build-it/) for this.
For other operating systems, the [same page](/build-it/) page provides extra links with information.

### Contributing code

For contributions, the best way to hand them in is via our GitLab instance [invent.kde.org](https://invent.kde.org/utilities/kate).

How to set it up and use it is documented in the [invent.kde.org documentation](https://community.kde.org/Infrastructure/GitLab).

Just open a new [merge request](https://invent.kde.org/utilities/kate/merge_requests) there for Kate.
For more details about this, refer to [this blog post](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

If you want to get an overview about already accepted patches to get a feel how the process works, take a look at our [merge requests](/merge-requests/) page.
Visiting some of these accepted requests might help to have a better grasp of project's workings.

### Areas of work

The main work area is the programming, but if you are not a programmer you can help in many other areas:

  * Write and maintain documentation.
  * Write and maintain syntax highlight files.
  * Write and maintain scripts.
  * Maintain bug reports.
  * Provide us with useful feedback.
  * Help us by helping <a href="http://www.kde.org" target="_blank">KDE</a>. As a part of KDE, Kate benefits from the KDE as a whole, apart from the excellent integration provided by KDE the help from the <a href="http://i18n.kde.org" target="_blank">KDE translation team</a> is invaluable.

### C/C++ Coding Standards

The code in our repositories shall follow the style described for <a href="https://community.kde.org/Policies/Frameworks_Coding_Style" target="_blank">KDE Frameworks</a>.

The CMake tooling we use will setup appropriate pre-commit hooks to ensure this style using **clang-format**.

### Documenting your code

We use Doxygen syntax to document code, and it&#8217;s nice to document everything, even private code, since other people than you might get to work on it or just try to understand it.

For comments inside of functions, C++ single-line comments are preferred over
multi-line C comments.

### Good starting points

You can take a look at our current list of issues, perhaps you are interested in taking care of one.

* <a title="Bug Charts for KSyntaxHighlighting" href="https://bugs.kde.org/reports.cgi?product_id=754&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for KSyntaxHighlighting</a>
* <a title="Bug Charts for KTextEditor" href="https://bugs.kde.org/reports.cgi?product_id=623&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for KTextEditor</a>
* <a title="Bug Charts for Kate and KWrite" href="https://bugs.kde.org/reports.cgi?product_id=39&#038;datasets=UNCONFIRMED&#038;datasets=CONFIRMED&#038;datasets=ASSIGNED&#038;datasets=REOPENED" target="_blank">Bug Charts for Kate/KWrite</a>
* <a title="KSyntaxHighlighting Bug Tracker" href="http://bugs.kde.org/buglist.cgi?product=frameworks-syntax-highlighting&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor">KSyntaxHighlighting Bug Tracker</a>
* <a title="Kate/KWrite Bug Tracker" href="http://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor">Kate and KWrite Bug Tracker</a>
* <a title="Kate/KWrite Wishlist" href="http://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist">Feature Wishlist for Kate and KWrite</a>
* <a title="Weekly Summary for Kate/KWrite" href="http://bugs.kde.org/weekly-bug-summary.cgi">Weekly Summary for KDE Bugs</a>

 [1]: mailto:kwrite-devel@kde.org
