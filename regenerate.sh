#!/usr/bin/env bash

# failures are evil
set -e

# update team statistics
./the-team-update.pl
git add content/the-team.md

# update merge request statistics
./merge-requests-update.py
